#include <iostream>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include "mydepend/tinyxml.h"
#include <sys/time.h>
#include <fcntl.h>
#include<errno.h>
#include<sched.h>

#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include "shmfifo.h"
#include "./cLog/log.h"


using namespace std;
using namespace cv;

#define GAP_DATA_LEN (600*600)

typedef struct
{
    int width;
    int height;
    char data[GAP_DATA_LEN + 1];
} GAPImage;

typedef struct
{
    int capInterval;
    int capWidth;
    int capHeigth;
    int splitX;
    int splitY;
    int splitWidth;
    int splitHeight;
    int camID1;
    int camID2;
    int camID3;
    int camID4;
    string outPutPath1;
    string outPutPath2;
    string outPutPath3;
    string outPutPath4;
    int exposureAbs;
} CaptureConfig;

CaptureConfig _config;

typedef struct shm
{
    void *shm_head;
    key_t key;
    int shmid;
} shm_t;

vector<shmfifo_t *> vec_shm;

bool loadConfig(string path);
shmfifo_t * getCaptureMemory(string path, unsigned int blksize, unsigned int blocks);
void releaseCaptureMemory(shmfifo_t *fifo);
void checkFile(string path);
static int cachelength = 500;
int SetExposure(int camIndex,int exposureAbs);

extern  int fd ;
int read_frame (Mat &frame);
int SetCamera(int camIndex);
void Release_Resource(void);

struct buffer {
        void *                  start;
        size_t                  length;
};

extern struct buffer * buffers;
extern unsigned int  n_buffers;

void signal_handler(int s)
{
    char buffer[100];
    printf("Getted signal id=%d\n",s);
    for (int i = 0; i < vec_shm.size(); ++i)
    {
        releaseCaptureMemory(vec_shm[i]);
    }
    vec_shm.clear();
    _exit(0);
}

int main(int argc,char* argv[])
{
    signal(SIGINT,signal_handler);//Hook for ctrl+c and other key envents
    signal(SIGKILL,signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGPIPE,SIG_IGN);

    //日志等级　LL_DEBUG<LL_TRACE<LL_NOTICE<LL_WARNING<LL_ERROR
    bool LogInit(LogLevel l);
    LogInit(LL_DEBUG);
    shm_t shm;

    //以前有４个摄像头，现在只有２个摄像头了，所以参数只能是１或２
    if(argc!=2)
    {
        printf("please input parameter (1 or 2 or 3 or 4)    \n");
        LOG_WARN("please input parameter (1 or 2 or 3 or 4)    \n");
        return -100;
    }

    string version = "3.5.0";
    if(strcmp(argv[1],"-v")==0)
    {
        cout<<version<<endl;
        return -100;
    }

    if(!loadConfig("CaptureConfig.xml"))
    {
        cout << "Reading configuration file failed" << endl;
        LOG_ERROR("Reading configuration file failed    \n");
        return -1;
    }

    GAPImage cur_qrcode_data;
    shmfifo_t *fifo = NULL;

    struct sched_param param;
    int maxpri=sched_get_priority_max(SCHED_FIFO);
    if(maxpri!=-1)
    {
        param.__sched_priority=maxpri;
        if(sched_setscheduler(getpid(),SCHED_FIFO,&param)==-1)
        {
            printf("sched_setscheduler() failed");
            LOG_ERROR("sched_setscheduler() failed    \n");
        }
    }
    else
    {
        printf("sched_get_priority_max() failed");
        LOG_ERROR("sched_get_priority_max() failed    \n");
    }

    int camflg =atoi(argv[1]);

    switch(camflg)
    {
    case 1:
        checkFile(_config.outPutPath1);
        fifo = getCaptureMemory(_config.outPutPath1, sizeof(GAPImage), cachelength);
        vec_shm.push_back(fifo);
        break;
    case 2:
        checkFile(_config.outPutPath2);
        fifo = getCaptureMemory(_config.outPutPath2, sizeof(GAPImage), cachelength);
        vec_shm.push_back(fifo);
        break;
    case 3:
        checkFile(_config.outPutPath2);
        fifo = getCaptureMemory(_config.outPutPath2, sizeof(GAPImage), cachelength);
        vec_shm.push_back(fifo);
        break;
    case 4:
        checkFile(_config.outPutPath2);
        fifo = getCaptureMemory(_config.outPutPath2, sizeof(GAPImage), cachelength);
        vec_shm.push_back(fifo);
        break;
    default:
        printf("please input parameter (1 or 2 or 3 or 4)    \n");
        LOG_ERROR("please input parameter (1 or 2 or 3 or 4)    \n");
        return -100;
    }

    if(fifo == NULL)
    {
        cout << "Apply for shared memory failed" << endl;
        LOG_ERROR("Apply for shared memory failed    \n");
        return -2;
    }

    SetCamera(camflg-1);

    int i = 0;
    Mat frame;
    Rect splitRect(_config.splitX,_config.splitY,_config.splitWidth,_config.splitHeight);
    IplImage* imgGray = cvCreateImage(cvSize(_config.splitWidth,_config.splitHeight),IPL_DEPTH_8U,1);

#ifdef _DEBUG
    struct timeval t1;
    int count = 0;

    gettimeofday(&t1,NULL);
#endif

    //frame =cvQueryFrame(capture);
    //SetExposure(camflg-1,_config.exposureAbs);
    while(1)
    {
        for (;;) //这一段涉及到异步IO
        {
           fd_set fds;
           struct timeval tv;
           int r;

           FD_ZERO (&fds);//将指定的文件描述符集清空
           FD_SET (fd, &fds);//在文件描述符集合中增加一个新的文件描述符

           /* Timeout. */
           tv.tv_sec = 2;
           tv.tv_usec = 0;

           //判断是否可读（即摄像头是否准备好），tv是定时
           r = select (fd + 1, &fds, NULL, NULL, &tv);

           if (-1 == r)
           {
               if (EINTR == errno)
                   continue;
               printf ("select err\n");
           }

           if (0 == r)
           {
               fprintf (stderr, "select timeout\n");
               exit (EXIT_FAILURE);
           }

           //如果可读，执行read_frame ()函数，并跳出循环
           if (read_frame(frame))
               break;
        }

        try
        {
            IplImage imgSRC = IplImage(frame);
            cvSetImageROI(&imgSRC,splitRect);
            cvCvtColor(&imgSRC,imgGray,CV_RGB2GRAY);
            cvResetImageROI(&imgSRC);
        }
        catch(cv::Exception)
        {

        }

#ifdef _DEBUG
        count++;
        struct timeval t2;
        gettimeofday(&t2,NULL);
        float total = (t2.tv_sec-t1.tv_sec) + (t2.tv_usec-t1.tv_usec)/1000000;
        if(total>=0.01)
        {   printf("fps:%f\n",count/total);
            count=0;
            gettimeofday(&t1,NULL);
        }
#endif

        cur_qrcode_data.width = imgGray->width;
        cur_qrcode_data.height = imgGray->height;
        memcpy(cur_qrcode_data.data, imgGray->imageData, imgGray->imageSize);

        //blocked util it can be written
        shmfifo_put(fifo, &cur_qrcode_data);

        if(_config.capInterval>0) //摄像头扫描时间间隔
            usleep(_config.capInterval*1000);
    }

    cvReleaseImage(&imgGray);
    for (int i = 0; i < vec_shm.size(); ++i)
    {
        releaseCaptureMemory(vec_shm[i]);
    }

    cout << "Hello world!" << endl;
    return 0;
}

shmfifo_t * getCaptureMemory(string path, unsigned int blksize, unsigned int blocks)
{
    shmfifo_t *fifo = NULL;
    key_t key ;

    key = ftok(path.c_str(), 0);
    if (key == -1)
    {
        printf("%s\n", "ftok error!");
        LOG_ERROR("%s \n", "ftok error");
        exit(-1);
    }

    fifo = shmfifo_init(key, blksize, blocks);

    return fifo;
}

void releaseCaptureMemory(shmfifo_t *fifo)
{
    shmfifo_destroy(fifo);
}

bool loadConfig(string path)
{
    TiXmlDocument doc(path.c_str());
    bool loadOkay = doc.LoadFile();
    if(loadOkay)
    {
        TiXmlElement* e0 = doc.RootElement();
        TiXmlElement* e1 = e0->FirstChildElement("capInterval");
        TiXmlAttribute* a1 = e1->FirstAttribute();
        string tmp = a1->Value();
        _config.capInterval = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("capWidth");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.capWidth = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("capHeight");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.capHeigth = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("splitX");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.splitX = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("splitY");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.splitY = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("splitWidth");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.splitWidth = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("splitHeight");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.splitHeight = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("camID1");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.camID1 = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("camID2");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.camID2 = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("camID3");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.camID3 = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("camID4");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.camID4 = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("outPutPath1");
        a1 = e1->FirstAttribute();
        _config.outPutPath1 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath2");
        a1 = e1->FirstAttribute();
        _config.outPutPath2 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath3");
        a1 = e1->FirstAttribute();
        _config.outPutPath3 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath4");
        a1 = e1->FirstAttribute();
        _config.outPutPath4 = a1->Value();

        e1 = e0->FirstChildElement("exposureAbs");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.exposureAbs = atoi(tmp.c_str());

    }

    return loadOkay;
}

void checkFile(string path)
{
    if(access(path.c_str(),F_OK|R_OK)!=0)
    {
        int fd = open(path.c_str(),O_CREAT,O_RDWR);
        close(fd);
    }
    else
    {
        printf("%s\n"," keyfileaccess error");
        LOG_ERROR("keyfileaccess error \n");
    }
}

int SetExposure(int camIndex,int exposureAbs)
{
    stringstream device;
    device<< "/dev/video";
    device<<camIndex;

    int fd,ret;

    fd = open(device.str().c_str(), O_RDWR);
    if (fd < 0)
    {
        printf("Open %s failed\n", device.str().c_str());
        LOG_ERROR("Open %s failed \n", device.str().c_str());
        return fd;
    }

    struct v4l2_control ctrl;

    //set manual exposure
    ctrl.id = V4L2_CID_EXPOSURE_AUTO;
    ctrl.value = V4L2_EXPOSURE_MANUAL;
    ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
    if (ret < 0)
    {
        printf("set exposure manual failed (%d)\n", ret);
        LOG_ERROR("set exposure manual failed (%d)\n", ret);
        return ret;
    }

    //set exposure absolute
    ctrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;
    ctrl.value = exposureAbs;
    ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
    if (ret < 0)
    {
        printf("set exposure absolute failed (%d)\n", ret);
        LOG_ERROR("set exposure absolute failed (%d)\n", ret);
        return ret;
    }

    //set aperture priority
    ctrl.id = V4L2_CID_EXPOSURE_AUTO;
    ctrl.value = V4L2_EXPOSURE_APERTURE_PRIORITY;
    ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
    if (ret < 0)
    {
        printf("set aperture priority failed (%d)\n", ret);
        LOG_ERROR("set aperture priority failed (%d)\n", ret);
        return ret;
    }

    //set manual exposure
    ctrl.id = V4L2_CID_EXPOSURE_AUTO;
    ctrl.value = V4L2_EXPOSURE_MANUAL;
    ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
    if (ret < 0)
    {
        printf("set exposure manual failed (%d)\n", ret);
        LOG_ERROR("set exposure manual failed (%d)\n", ret);
        return ret;
    }

    //close Device
    close(fd);
    return 1;
}

bool LogInit(LogLevel l)
{
    //variable to construct the file name which including time stamp
    char makeFileName[64];

    time_t tmp = time(NULL);
    tm* tmpTime = localtime(&tmp);

    //detail filename, accurate to seconds
    snprintf(makeFileName, 64,"%d%02d%02d_%02d%02d%02d",
             tmpTime->tm_year+1900,
             tmpTime->tm_mon+1,
             tmpTime->tm_mday,
             tmpTime->tm_hour,
             tmpTime->tm_min,
             tmpTime->tm_sec);

    string Path = "logs_camera/LOG_";
    Path.append(makeFileName);

    return log_init(l, Path.c_str(), "./");
}
