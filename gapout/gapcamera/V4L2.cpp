#include "V4L2.h"
#include <opencv/cv.h>

#include <opencv2/opencv.hpp>
using namespace cv;

//////////////////////////////////////////////////////
//获取一帧数据
//////////////////////////////////////////////////////
int Count ;
int read_frame (Mat &frame)
{
    struct v4l2_buffer buf;
    //unsigned int i;

    CLEAR (buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    ioctl (fd, VIDIOC_DQBUF, &buf); //出列采集的帧缓冲

    assert (buf.index < n_buffers);
    //printf ("buf.index dq is %d,\n",buf.index);
    //printf("Image Length: %d\n", buf.bytesused);

    //fwrite(buffers[buf.index].start, buf.bytesused, 1, file_fd); //将其写入文件中
    //cout<<"Run here1"<<endl;
    vector < char> ImageTemp;
    //vector < char> ImageTemp(buf.bytesused);
    ImageTemp.assign(buffers[buf.index].start,buffers[buf.index].start+buf.bytesused);
    //for(int i=0;i<buf.bytesused;i++)
    //{
     //   ImageTemp[i]= *(buffers[buf.index].start+i);
    //}
    //CvMat img = CvMat(ImageTemp);
    //cout<<"Run here2"<<endl;
    frame = imdecode(ImageTemp,CV_LOAD_IMAGE_COLOR);

    //memcpy(p_imageData,buffers[buf.index].start,buf.bytesused);

    ioctl (fd, VIDIOC_QBUF, &buf); //再将其入列

    return 1;
}

int SetCamera(int camIndex)
{
   stringstream device;
    device<< "/dev/video";
    device<<camIndex;

    //int fd,ret;
    //file_fd = fopen("test-mmap0.jpg", "w");//图片文件名

    fd = open(device.str().c_str(),  O_RDWR /* required */ | O_NONBLOCK,0);
    if (fd < 0)
    {
        printf("Open %s failed\n", device.str().c_str());
        return fd;
    }

    struct v4l2_capability cap;
    struct v4l2_format fmt;
    unsigned int i;
    enum v4l2_buf_type type;


    ioctl (fd, VIDIOC_QUERYCAP, &cap);//获取摄像头参数

      printf( "Driver Caps:\n"
        "  Driver: \"%s\"\n"
        "  Card: \"%s\"\n"
        "  Bus: \"%s\"\n"
        "  Version: %d.%d\n"
        "  Capabilities: %08x\n",
        cap.driver,
        cap.card,
        cap.bus_info,
        (cap.version>>16)&&0xff,
        (cap.version>>24)&&0xff,
        cap.capabilities);
        if((cap.capabilities&V4L2_CAP_VIDEO_CAPTURE)==V4L2_CAP_VIDEO_CAPTURE) //表示是一个视频捕捉设备
        {
            printf("Device %s:supports capture.\n",FILE_VIDEO);
        }
        if((cap.capabilities&V4L2_CAP_STREAMING)==V4L2_CAP_STREAMING) //并且具有数据流控制模式
        {
            printf("Device %s:supports streaming.\n",FILE_VIDEO);
        }


/////////////////////////////
   struct v4l2_fmtdesc fmtdesc = {0};
    fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE; //常见的捕获模式，即设置视频捕捉模式
    printf("Support format:\n");
    while (ioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc)!=-1)  //获取当前驱动支持的视频格式
    {
        printf("\t%d.%s\n",fmtdesc.index+1,fmtdesc.description);
        fmtdesc.index++;
    }
//////////////////////////////////
    CLEAR (fmt);
    fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = IMAGEWIDTH;
    fmt.fmt.pix.height      = IMAGEHEIGHT;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
    fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;
    if(ioctl (fd, VIDIOC_S_FMT, &fmt)==-1)//设置图像格式　设置捕获视频的格式
    {
        printf("unable to set format \n");
    }
    if(ioctl(fd,VIDIOC_G_FMT,&fmt)==-1) //获取支持的视频格式
    {
        printf("Unable to get format\n");
    }
    char fourcc[5] = {0};
    strncpy(fourcc, (char *)&fmt.fmt.pix.pixelformat, 4);
    printf( "Selected Camera Mode:\n"
        "  Width: %d\n"
        "  Height: %d\n"
        "  PixFmt: %s\n"
        "  Field: %d\n",
        fmt.fmt.pix.width,
        fmt.fmt.pix.height,
        fourcc,
        fmt.fmt.pix.field);

    file_length = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height; //计算图片大小


    struct v4l2_requestbuffers req;
    CLEAR (req);
    req.count               = 4;
    req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory              = V4L2_MEMORY_MMAP;

    if(ioctl (fd, VIDIOC_REQBUFS, &req)==-1)//申请缓冲，count是申请的数量　向驱动提出申请内存的请求
    {
         printf("request for buffers error\n");
    }
    if (req.count < 2)
       printf("Insufficient buffer memory\n");

    buffers =(buffer *) calloc (req.count, sizeof (*buffers));//内存中建立对应空间

    for (n_buffers = 0; n_buffers < req.count; ++n_buffers)
    {
       struct v4l2_buffer buf;   //驱动中的一帧
       CLEAR (buf);
       buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
       buf.memory      = V4L2_MEMORY_MMAP;
       buf.index       = n_buffers;

       if (-1 == ioctl (fd, VIDIOC_QUERYBUF, &buf)) //映射用户空间
        printf ("VIDIOC_QUERYBUF error\n");

       buffers[n_buffers].length = buf.length;
       buffers[n_buffers].start =
       (char*)mmap (NULL /* start anywhere */,    //通过mmap建立映射关系
        buf.length,
        PROT_READ | PROT_WRITE /* required */,
        MAP_SHARED /* recommended */,
        fd, buf.m.offset);

       if (MAP_FAILED == buffers[n_buffers].start)
        printf ("mmap failed\n");
            }

    for (i = 0; i < n_buffers; ++i)
    {
       struct v4l2_buffer buf;
       CLEAR (buf);

       buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
       buf.memory      = V4L2_MEMORY_MMAP;
       buf.index       = i;

       if (-1 == ioctl (fd, VIDIOC_QBUF, &buf))//申请到的缓冲进入列队
        printf ("VIDIOC_QBUF failed\n");
    }

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == ioctl (fd, VIDIOC_STREAMON, &type)) //开始捕捉图像数据　打开视频流
       printf ("VIDIOC_STREAMON failed:%s\n",strerror(errno));
    return 0;
}

void Release_Resource(void)
{
    for (unsigned int i = 0; i < n_buffers; ++i)
       if (-1 == munmap (buffers[i].start, buffers[i].length))
        printf ("munmap error");
    close (fd);
    fclose (file_fd);
    exit (EXIT_SUCCESS);
}
