#ifndef __V4L2_h__
#define __V4L2_h__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <getopt.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>
#include <linux/videodev2.h>
#include <stdbool.h>
#include <iostream>
#include <sstream>
#include <vector>

#define CLEAR(x) memset (&(x), 0, sizeof (x))

 #define  TRUE    1
 #define FALSE 0
 #define FILE_VIDEO     "/dev/video0"
 #define BMP          "image_bmp.bmp"
 #define YUV            "image_yuv.yuv"

 #define  IMAGEWIDTH    640
 #define  IMAGEHEIGHT   480
using namespace std;


// static   struct   v4l2_capability   cap;   //delete by szz
 struct v4l2_fmtdesc fmtdesc;
 struct v4l2_format fmt,fmtack;
 struct v4l2_streamparm setfps;
 struct v4l2_requestbuffers req;
 struct v4l2_buffer buf;
 enum v4l2_buf_type type;
 unsigned char frame_buffer[IMAGEWIDTH*IMAGEHEIGHT*3];

struct buffer {
        char *                  start;
        size_t                  length;
};

//static char *           dev_name0        = "/dev/video0";//摄像头设备名
//static char *           dev_name1        = "/dev/video1";//摄像头设备名
//static char *           dev_name2        = "/dev/video2";//摄像头设备名
 int fd   = -1;
 struct buffer * buffers = NULL;
 unsigned int  n_buffers = 0;

 FILE *file_fd;
 unsigned long file_length;
 unsigned char *file_name;

 typedef unsigned char uchar;
 typedef unsigned int uint;
 int read_frame (char *p_imageData);
 int SetCamera(int camIndex);

#endif // __V4L2_h__
