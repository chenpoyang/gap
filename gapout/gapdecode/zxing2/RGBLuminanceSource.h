#ifndef RGBLUMINANCESOURCE_H
#define RGBLUMINANCESOURCE_H

#include "LuminanceSource.h"

namespace zxing2
{
class RGBLuminanceSource : public LuminanceSource
{
public:

    //RGBLuminanceSource(unsigned char* d, int W, int H);
    RGBLuminanceSource(char* d, int W, int H);
    RGBLuminanceSource(char* d, int W, int H,int maxGray,int minGray);
    //RGBLuminanceSource(unsigned char* d, int W, int H,bool Is8Bit);
    //RGBLuminanceSource(unsigned char* d, int W, int H, bool Is8Bit,Rectangle Region);
    //RGBLuminanceSource(Bitmap d, int W, int H);

    virtual ~RGBLuminanceSource();
    //unsigned char* getRow(int y, unsigned char* row);
    ArrayRef<char> getRow(int y, ArrayRef<char> row)const;
    //char* getRow(int y, char* row,int rowLength);
    ArrayRef<char> getMatrix()const;
    //char* getMatrix();
    Ref<LuminanceSource> crop(int left, int top, int width, int height);
    Ref<LuminanceSource> rotateCounterClockwise();
    bool isRotateSupported() ;

    bool isQRcodeImage();
    int getHeight() const;
    int getWidth() const;


protected:
private:
    int __width;
    int __height;
    char* luminances;
    //ArrayRef<char> luminances;
    bool isRotated;
    bool _isQRcodeImage;


};
}

#endif // RGBLUMINANCESOURCE_H
