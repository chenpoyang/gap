#ifndef __MULTI_DETECTOR_H__
#define __MULTI_DETECTOR_H__

/*
 *  Copyright 2011 zxing2 authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <zxing2/qrcode/detector/Detector.h>
#include <zxing2/common/DetectorResult.h>
#include <zxing2/DecodeHints.h>

namespace zxing2 {
namespace multi {

class MultiDetector : public zxing2::qrcode::Detector {
  public:
    MultiDetector(Ref<BitMatrix> image);
    virtual ~MultiDetector();
    virtual std::vector<Ref<DetectorResult> > detectMulti(DecodeHints hints);
};

}
}

#endif // __MULTI_DETECTOR_H__
