#include <iostream>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <deque>
#include <linux/kd.h>
#include <sys/time.h>
#include "mydepend/tinyxml.h"
#include "qrcode/QRCodeReader.h"
#include "RGBLuminanceSource.h"
#include "LuminanceSource.h"
#include "common/HybridBinarizer.h"
#include "Result.h"

#include "zxing/qrcode/QRCodeReader.h"
#include "zxing/RGBLuminanceSource.h"
#include "zxing/LuminanceSource.h"
#include "zxing/common/HybridBinarizer.h"
#include "zxing/Result.h"

#include <pthread.h>
#include <stdio.h>
#include <math.h>
#include <uuid.h>

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <signal.h>
#include <stdlib.h>
#include "./cLog/log.h"
#include "shmfifo.h"

using namespace std;
using namespace cv;
using namespace zxing2;
using namespace zxing2::qrcode;

string DEST_IP = "127.0.0.1";
#define DEST_PORT 8081
#define GAP_DATA_LEN (600*600)

typedef struct
{
    int width;
    int height;
    char data[GAP_DATA_LEN + 1];
} GAPImage;

typedef struct
{
    int interval;
    int maxGray;
    int minGray;
    string inPutPath1;
    string inPutPath2;
    string inPutPath3;
    string inPutPath4;

    //两个decode后的数据要通过一个共享将数据进行合并归一.
    string str_slice_merge;
    string file_slice_merge;
} DecodeConfig;

typedef struct
{
    char data[GAP_DATA_LEN + 1];
} decoded_data_t;

//decode1 and decode2 data merge's share memory
shmfifo_t *fifo_str = NULL, *fifo_file = NULL;

vector<shmfifo_t *> vec_shm;
pthread_t retry, _sendT, sendF;
pthread_attr_t _sendT_attr, sendF_attr;
// pthread_mutex_t _sendMutex = PTHREAD_MUTEX_INITIALIZER; //for string cache
pthread_mutex_t _retry_mutex = PTHREAD_MUTEX_INITIALIZER;    //for retry cache
// pthread_mutex_t _fileSendMutex = PTHREAD_MUTEX_INITIALIZER; //for file cache
long errorcount=0;
int cachelength=500;
int sockfd = -1;
//zxing::qrcode::QRCodeReader _qrcode;
DecodeHints _hints;
deque<string> _sendCache;   //sting sendcache
deque<string> _fileSendCache;  //file sendcache
deque<Mat> _retryCache; //
DecodeConfig _config;

bool loadConfig(string path);

shmfifo_t * getCaptureMemory(const string path,
                               unsigned int blksize,
                               unsigned int blocks);

void releaseCaptureMemory(shmfifo_t * fifo);
bool sendDataSocket(string &content);
void checkFile(string path);
bool _QRDecode(char* data,int w,int h,string &res);
short int _QRDecode(char* data,int w,int h,int maxGray,int minGray,string &res);
void *retryThread(void *param);
void *sendThread(void *param);
void *sendFileThread(void *param);
int get_pkg_length(const char *head, const int len); //每个分包前len位表示接下来的字节数
int writeToFile(const char *buf);

//对收到的分包进行校验，收到的不全就丢掉(用于对两个decode后数据整合前进行过滤)
bool received_file_pkg_is_complete(const string &buf);

void signal_handler(int s)
{
    printf("Getted signal id=%d\n",s);

    for (int i = 0; i < vec_shm.size(); ++i)
    {
        releaseCaptureMemory(vec_shm[i]);
    }
    vec_shm.clear();

    shmfifo_destroy(fifo_str);
    shmfifo_destroy(fifo_file);
    fifo_str = NULL;
    fifo_file = NULL;

    close(sockfd);
    _exit(0);
}

/********************szz***************************/
#define     WarningBeepFreq    11900000/5000
void  *warningBeep_Pthread(void *param)
{
    int console_fd;
    int times = 3;
    console_fd = open("/dev/console",O_WRONLY, 0777);   //open the device

    cout<<"BeepTimes= "<<times<<endl;

    for(int i=0;i<times;i++)
    {
        ioctl(console_fd,KIOCSOUND,WarningBeepFreq);
        usleep(250*1000);
        ioctl(console_fd,KIOCSOUND,0);
        usleep(50*1000);
        ioctl(console_fd,KIOCSOUND,WarningBeepFreq);
        usleep(250*1000);
        ioctl(console_fd,KIOCSOUND,0);
        usleep(500*1000);
    }

    close(console_fd);

    return 0;
}

int initSocket(void)
{
    struct sockaddr_in dest_addr;
    int tmpSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (tmpSocket == -1)
    {
        perror("socket()");
        LOG_ERROR("socket error..\n");
        return -1;
    }
    /* 设置远程连接的信息*/
    dest_addr.sin_family = AF_INET; /* 注意主机字节顺序*/
    dest_addr.sin_port = htons(DEST_PORT); /* 远程连接端口, 注意网络字节顺序*/
    /* 远程 IP 地址, inet_addr() 会返回网络字节顺序*/
    dest_addr.sin_addr.s_addr = inet_addr(DEST_IP.c_str());
    bzero(&(dest_addr.sin_zero), 8); /* 其余结构须置 0*/

    int res = connect(tmpSocket,
                      (struct sockaddr *)&dest_addr,
                      sizeof(struct sockaddr_in));
    if (res == -1)
    {
        perror("connect error");
        close(tmpSocket);
        return -1;
    }
    return tmpSocket;
}

bool sendDataSocket(char *content)
{
    char* data_byte = content;

    if(send(sockfd,data_byte,strlen(data_byte),0)==-1)
    {
        perror("send error");
        LOG_ERROR("send error\n");
        close(sockfd);

        return false;
    }

    char recvStr[512] = {0};  //szz
    int res = recv(sockfd,recvStr,sizeof(recvStr),0);  //block,
    cout<<"RECV:"<<recvStr<<endl;
    if (res == -1)
    {
        perror("recv error");
        LOG_ERROR("recv error\n");
        close(sockfd);
        return false;
    }

    if(recvStr[4] == 'w')   //Get warning message
    {
        LOG_WARN("Get warning message!\n");
        pthread_t warning_beep;
        pthread_create(&warning_beep,NULL,warningBeep_Pthread,NULL);
        pthread_detach(warning_beep);  //wait pthread end and distory it
    }

    return true;
}

int main(int argc,char* argv[])
{
    //Hook for ctrl+c and other key envents
    signal(SIGINT,signal_handler);
    signal(SIGKILL,signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGPIPE,SIG_IGN);

    //日志等级　LL_DEBUG<LL_TRACE<LL_NOTICE<LL_WARNING<LL_ERROR
    bool LogInit(LogLevel l);
    LogInit(LL_DEBUG);
    shmfifo_t * fifo = NULL;

    if(argc==1)   //change by szz
    {
        printf("please input parameter (1 or 2 or -v)    \n");
        LOG_WARN("please input parameter (1 or 2 or -v)    \n");
        return -100;
    }
    string version = "3.5.0";
    if(strcmp(argv[1],"-v")==0)
    {
        cout<<version<<endl;
        return -100;
    }

    if(argc==4&&(strcmp(argv[2],"-debug")==0)) //change by szz
    {
        DEST_IP=argv[3];
        cout<<"Debug Mode:"<<endl<<"DestIp is:"<<DEST_IP<<endl;
        LOG_DEBUG("Debug Mode: DestIp is %s\n", DEST_IP.c_str());
    }

    if(!loadConfig("DecodeConfig.xml"))return -2;

    int decodeflg =atoi(argv[1]);

    key_t key;

    checkFile(_config.str_slice_merge);
    key = ftok(_config.str_slice_merge.c_str(), 0);
    fifo_str = shmfifo_init(key, sizeof(decoded_data_t), cachelength);

    checkFile(_config.file_slice_merge);
    key = ftok(_config.file_slice_merge.c_str(), 0);
    fifo_file = shmfifo_init(key, sizeof(decoded_data_t), cachelength);

    //there is 4 camera before, now just 1/2, ignore 3/4.
    switch(decodeflg)
    {
        case 1:
            checkFile(_config.inPutPath1);
            fifo = getCaptureMemory(_config.inPutPath1, sizeof(GAPImage), cachelength);
            vec_shm.push_back(fifo);

            pthread_attr_init(&_sendT_attr);
            pthread_attr_init(&sendF_attr);
            pthread_attr_setdetachstate(&_sendT_attr,PTHREAD_CREATE_DETACHED);
            pthread_attr_setdetachstate(&sendF_attr,PTHREAD_CREATE_DETACHED);
            pthread_create(&_sendT,NULL,sendThread,fifo_str);
            pthread_create(&sendF,NULL,sendFileThread,fifo_file);

            break;
        case 2:
            checkFile(_config.inPutPath2);
            fifo = getCaptureMemory(_config.inPutPath2, sizeof(GAPImage), cachelength);
            vec_shm.push_back(fifo);

            break;
        case 3:
            checkFile(_config.inPutPath3);
            fifo = getCaptureMemory(_config.inPutPath3, sizeof(GAPImage), cachelength);
            vec_shm.push_back(fifo);

            break;
        case 4:
            checkFile(_config.inPutPath4);
            fifo = getCaptureMemory(_config.inPutPath4, sizeof(GAPImage), cachelength);
            vec_shm.push_back(fifo);

            break;
        default:
            printf("please input parameter (1 or 2)    \n");
            LOG_WARN("please input parameter (1 or 2)    \n");
            return -100;
    }

    sockfd = initSocket();
    pthread_create(&retry,NULL,retryThread,NULL);

    int offset = 0;
    GAPImage cur_qrcode_data;
    short int decodeStatus = 0;

    string res;
    while(1)
    {
        //block util data is available to be read.
        shmfifo_get(fifo, &cur_qrcode_data);

        decodeStatus = _QRDecode(cur_qrcode_data.data,
                                 cur_qrcode_data.width,
                                 cur_qrcode_data.height,
                                 _config.maxGray,
                                 _config.minGray,
                                 res);

        if(decodeStatus > 0)
        {
            if(res.c_str()[4] == 'F')
            {
                decoded_data_t data;
                strcpy(data.data, res.c_str());
                shmfifo_put(fifo_file, &data);
            }
            else if(res.c_str()[4] == '4')  //Heart beat
            {
                char *HBdata = (char *)res.c_str();
                if(!sendDataSocket(HBdata))
                {
                    sockfd = initSocket();
                }
            }
            else if(res.c_str()[4] == '1'
                    || res.c_str()[4] == '2'
                    || res.c_str()[4] == '3')
            {
                // pthread_mutex_lock(&_sendMutex);
                // _sendCache.push_back(res);
                // pthread_mutex_unlock(&_sendMutex);
                decoded_data_t data;
                strcpy(data.data, res.c_str());
                shmfifo_put(fifo_str, &data);
            }

        }
        else if(decodeStatus == 0)     //decode fail
        {

#ifdef DEBUG
            cout<<"decodeStatus"<<endl;
            IplImage* imgGray = cvCreateImageHeader(cvSize(cur_p->width,cur_p->height),IPL_DEPTH_8U,1);
            cvSetData(imgGray,cur_p->data,cur_p->width);
            stringstream ss;
            CvScalar avg = cvAvg(imgGray);
            double avgVal = *avg.val;
            ss<<decodeflg;
            ss<<"_1_";
            ss<<errorcount;
            ss<< "_";
            ss<< avgVal;
            string strdst = "/home/decodetemp/";
            strdst += ss.str();
            strdst+=".bmp";
            cvSaveImage(strdst.c_str(),imgGray);
#endif

            IplImage* img = cvCreateImageHeader(cvSize(cur_qrcode_data.width, cur_qrcode_data.height), IPL_DEPTH_8U, 1);
            cvSetData(img, cur_qrcode_data.data, cur_qrcode_data.width);
            Mat mat(img,1);
            pthread_mutex_lock(&_retry_mutex);
            _retryCache.push_back(mat);
            pthread_mutex_unlock(&_retry_mutex);
            cvReleaseImageHeader(&img);
        }
    }

    for (int i = 0; i < vec_shm.size(); ++i)
    {
        releaseCaptureMemory(vec_shm[i]);
    }
    vec_shm.clear();

    //decode1,decode2,sendThread/sendFileThread (str/file share memory release)
    shmfifo_destroy(fifo_str);
    shmfifo_destroy(fifo_file);

    close(sockfd);

    pthread_cancel(retry);
    pthread_mutex_destroy(&_retry_mutex);

    if (decodeflg == 1)
    {
        pthread_cancel(_sendT);
        pthread_cancel(sendF);
    }

    cout << "Exit!" << endl;
    return 0;
}

shmfifo_t * getCaptureMemory(const string path,
                               unsigned int blksize,
                             unsigned int blocks)
{
    shmfifo_t * fifo = NULL;
    key_t key;

    key = ftok(path.c_str(), 0);
    if (key == -1)
    {
        printf("%s\n", "ftok error!");
        LOG_ERROR("%s \n", "ftok error");
        exit(-1);
    }

    fifo = shmfifo_init(key, blksize, blocks);

    return fifo;
}

void releaseCaptureMemory(shmfifo_t * fifo)
{
    shmfifo_destroy(fifo);
}

bool loadConfig(string path)
{
    TiXmlDocument doc(path.c_str());
    bool loadOkay = doc.LoadFile();
    if(loadOkay)
    {
        TiXmlElement* e0 = doc.RootElement();
        TiXmlElement* e1 = e0->FirstChildElement("interval");
        TiXmlAttribute* a1 = e1->FirstAttribute();
        string tmp = a1->Value();
        _config.interval = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("maxGray");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.maxGray = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("minGray");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.minGray = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("inPutPath1");
        a1 = e1->FirstAttribute();
        _config.inPutPath1 = a1->Value();

        e1 = e0->FirstChildElement("inPutPath2");
        a1 = e1->FirstAttribute();
        _config.inPutPath2 = a1->Value();

        e1 = e0->FirstChildElement("inPutPath3");
        a1 = e1->FirstAttribute();
        _config.inPutPath3 = a1->Value();

        e1 = e0->FirstChildElement("inPutPath4");
        a1 = e1->FirstAttribute();
        _config.inPutPath4 = a1->Value();

        e1 = e0->FirstChildElement("str_slice_merge");
        a1 = e1->FirstAttribute();
        _config.str_slice_merge = a1->Value();

        e1 = e0->FirstChildElement("file_slice_merge");
        a1 = e1->FirstAttribute();
        _config.file_slice_merge = a1->Value();
    }
    return loadOkay;
}

short int _QRDecode(char* data,int w,int h,int maxGray,int minGray,string &res)
{
    zxing::Ref<zxing::RGBLuminanceSource> source(new zxing::RGBLuminanceSource(data,w,h,maxGray,minGray));

    if(source->isQRcodeImage())
    {
        using namespace zxing;

        zxing::Ref<zxing::HybridBinarizer> hy(new zxing::HybridBinarizer(source));

        zxing::Ref<zxing::BinaryBitmap> bitmap(new zxing::BinaryBitmap(hy));

        zxing::DecodeHints _hints2;
        _hints2.setTryHarder(true);
        //Ref<Reader> _reader(new zxing2::qrcode::QRCodeReader());
        zxing::qrcode::QRCodeReader _qrcode;
        try
        {
            //pthread_mutex_lock(&_mutexDecode);
            zxing::Ref<zxing::Result> result(_qrcode.decode(bitmap,_hints2));
            //pthread_mutex_unlock(&_mutexDecode);
            res =  result->getText()->getText() ;

        }
        catch(zxing::Exception e)
        {
            //pthread_mutex_unlock(&_mutexDecode);
            return 0;//decode error
        }
        if(res.length()>=27)
        {
            return 1;
            //cout<<res<<endl;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return -1;//source image is not qrcode
    }

    return -1;
}

bool _QRDecode(char* data,int w,int h,string &res)
{
    Ref<RGBLuminanceSource> source(new RGBLuminanceSource(data,w,h));
    Ref<HybridBinarizer> hy(new HybridBinarizer(source));
    Ref<BinaryBitmap> bitmap(new BinaryBitmap(hy));
    _hints.setTryHarder(true);
    Ref<Reader> _reader(new zxing2::qrcode::QRCodeReader());
    try
    {
        //pthread_mutex_lock(&_mutexDecode);
        Ref<Result> result(_reader->decode(bitmap,_hints));
        //pthread_mutex_unlock(&_mutexDecode);
        res =  result->getText()->getText() ;
    }
    catch(zxing2::Exception e)
    {
        //pthread_mutex_unlock(&_mutexDecode);
        // printf("decode error \n");
        return false;
    }

    return (res.length()>=27);
}

//parameter 1,2,3  90,180,270 degeree
IplImage *rotate_image(IplImage *image, int _90_degrees_steps_anti_clockwise)
{
    IplImage *rotated;
    //if(_90_degrees_steps_anti_clockwise != 2)
    rotated = cvCreateImage(cvSize(image->height, image->width), image->depth, image->nChannels);
    //else
    // rotated = cvCloneImage(image);
    //if(_90_degrees_steps_anti_clockwise != 2)
    // cvTranspose(image, rotated);
    if(_90_degrees_steps_anti_clockwise == 3)
        cvFlip(image, rotated, 1);
    else if(_90_degrees_steps_anti_clockwise == 1)
        cvFlip(image, rotated,0);
    else if(_90_degrees_steps_anti_clockwise == 2)
        cvFlip(image, rotated, -1);
    return rotated;
}

int verifystr(char* data, int length)
{
    int crc16 = 0xffff;

    for (int i = 0;i < length;i++)
    {
        char tempx = data[i]&0xFF;
        crc16 = crc16 ^ tempx;
        for (int j = 0;j < 8;j++)
        {
            if (crc16 & 0x01)
                crc16 = (crc16 >> 1) ^ 0xa001;
            else crc16 = crc16 >> 1;
        }
    }
    return crc16;
}

int strToInt(char *buff,int length)
{
    char ptr[2] = {0};
    int res = 0;

    if(buff[0] < 0x3A && buff[0] > 0x2F)
        ptr[0] = buff[0]-0x30;
    else if(buff[0] > 0x60 && buff[0] < 0x67)
        ptr[0] = buff[0] - 87;

    if(buff[1] < 0x3A && buff[0] > 0x2F)
        ptr[1] = buff[1]-0x30;
    else if(buff[1] > 0x60 && buff[1] < 0x67)
        ptr[1] = buff[1] - 87;

    res = ptr[0]*10 + ptr[1];

    return res;

}

int strToHex(char *buff,int length)
{
    char ptr[2] = {0};
    int res = 0;

    if(buff[0] < 0x3A && buff[0] > 0x2F)
        ptr[0] = buff[0] - 0x30;
    else if(buff[0] > 0x60 && buff[0] < 0x67)
        ptr[0] = buff[0] - 87;

    if(buff[1] < 0x3A && buff[0] > 0x2F)
        ptr[1] = buff[1] - 0x30;
    else if(buff[1] > 0x60 && buff[1] < 0x67)
        ptr[1] = buff[1] - 87;

    res = ptr[0]*16 + ptr[1];

    return res;
}

typedef struct
{
    unsigned int fileID; //发过来的文件的唯一标识，组包要用
    //标识要组包的文件属性中，是否有分包数据，1:已经有，0:没有(是否已有数据)
    unsigned int status;
    unsigned int totalPackages;
    int timeVal;
    int nameLenth;
    char* name;
    char **content;
    char *packagesStatus;
}FileConf;

/*******************Content Form**********************
   4    1     4       2         2          x       1
 lenth cmd fileID totalPage currentPage content   crc
******************************************************/
/*
Parameters:
    content:
    sendstring:
Return:
    <-1:error
    -1:file package is exsist
     0:file is not complete
     >0:get a full file and return the fileID
     */
#define FileListNumber 50
#define FileTimeOutLimited 15
FileConf fileConfList[FileListNumber];  //File recv cache
int FiletimeOut = 0;
void FreeFileGroupSpace(int current_list_index);

/*
- [X] 传文件分包(文件属性及其内容)
|------------+--------+--------+-------------+----------------+--------------+------------------+----------|
|          4 |      1 |      4 |           2 |              2 | filename_len | file_content_len |        4 |
|------------+--------+--------+-------------+----------------+--------------+------------------+----------|
| pkg length | flag:F | fileid | total slice | slice sequence | filename     | file_content     | checksum |
|------------+--------+--------+-------------+----------------+--------------+------------------+----------|
*/
int fileGroup(string &content,char *fileBuff)
{
    //5: 前4个字节表示包的长度，第5位表示标记F.
    char *package_ptr = (char *)content.c_str()+5;
    int i=0,current_list=0;
    timeval tv1,tv2;

    /*detect if the file list timeout*/
    gettimeofday(&tv2,NULL);
    for(int j=0;j<FileListNumber;j++)
    {
        if(fileConfList[j].status == 1)
        {
            if(tv2.tv_sec - fileConfList[j].timeVal > FileTimeOutLimited)
            {
                ++FiletimeOut;
                cout<<"FileList:"<<j<<" ID:"<<fileConfList[j].fileID
                    <<" total:"<<fileConfList[j].totalPackages<<endl;
                LOG_WARN("File Timeout %d! fileID:%d,Name:%s,Total:%d",
                         FiletimeOut,
                         fileConfList[j].fileID,
                         fileConfList[current_list].name,
                         fileConfList[j].totalPackages);
                FreeFileGroupSpace(j);   //release the space
            }
        }
    }

    char p[2] = {0};
    unsigned int packages_id  = 0;
    p[0] = package_ptr[0];
    p[1] = package_ptr[1];
    unsigned int id_h = strToHex(p,2);

    p[0] = package_ptr[2];
    p[1] = package_ptr[3];
    unsigned int id_l = strToHex(p,2);

    packages_id = (id_h << 8)& 0xff00 | (id_l & 0xff);

    p[0] = package_ptr[4];
    p[1] = package_ptr[5];
    unsigned int totalPage = strToInt(p,2);  //get the file total packages

    p[0] = package_ptr[6];
    p[1] = package_ptr[7];
    unsigned int current_page = strToInt(p,2);

    char *ptr = NULL;

    /*******************Find the file list******************************/
    for(i=0;i<FileListNumber;i++)
    {
        //找到当前的分包属于那个文件数组元素
        if(fileConfList[i].fileID == packages_id)
        {
            if(fileConfList[i].status == 0) //todo
                return -2;

            current_list = i;
            fileConfList[current_list].fileID = packages_id;
            break;
        }
    }

    //此二维码的数据是文件的一部分，且为第一个要组的包.
    //如果在当前的文件数组中没有找到任何一个元素的fileID等于当前分包的文件id
    if(current_list == 0 && i==FileListNumber)
    {
        int j;

        for(j=0;j<FileListNumber;j++)
        {
            //status＝０表示这个文件数组元素还没有开始存储文件
            if(fileConfList[j].status == 0)
            {
                current_list = j;
                fileConfList[current_list].fileID = packages_id;
                break;
            }
        }

        //说明文件数组的每一个元素的status都等于１，即每一个元素都存储了文件
        if(j>=FileListNumber)
        {
            LOG_ERROR("The list is full!");
            return -3;
        }
    }

    //judge if is first packages
    if(fileConfList[current_list].status==0)
    {
        fileConfList[current_list].totalPackages = totalPage;
        fileConfList[current_list].status=1;
        ptr = &package_ptr[8];  //文件名＋文件内容
        int j=0;
        while(1)
        {
            //TODO:可能传的文件压根就没有后辍，而文件内容中有"."会出bug.
            if(ptr[j++] == '.')
                break;
        }

        //for filename,15bytes for time _20170810110430
        fileConfList[current_list].name = new char[j+32];
        memset(fileConfList[current_list].name,0,j+32);   //clear the buff

        time_t timep;
        struct tm *currentTime;
        time(&timep);
        currentTime = localtime(&timep);

        strncpy(fileConfList[current_list].name,ptr,j-1); //copy filename without '.'
        fileConfList[current_list].name[j-1] = '_';
        sprintf(fileConfList[current_list].name + j,"%04d",currentTime->tm_year+1900);
        sprintf(fileConfList[current_list].name + j+4,"%02d",currentTime->tm_mon+1);
        sprintf(fileConfList[current_list].name + j+4+2,"%02d",currentTime->tm_mday);
        sprintf(fileConfList[current_list].name + j+4+2+2,"%02d",currentTime->tm_hour);
        sprintf(fileConfList[current_list].name + j+4+2+2+2,"%02d",currentTime->tm_min);
        sprintf(fileConfList[current_list].name + j+4+2+2+2+2,"%02d",currentTime->tm_sec/10);
        strncpy(fileConfList[current_list].name+j-1+15,ptr+j-1,4); //copy filename with '.txt'

        fileConfList[current_list].nameLenth = j+3;

        fileConfList[current_list].packagesStatus = new char[fileConfList[current_list].totalPackages];
        fileConfList[current_list].content = new char *[fileConfList[current_list].totalPackages];
        for(unsigned int t=0;t<fileConfList[current_list].totalPackages;t++)
            fileConfList[current_list].content[t] = NULL;
        memset(fileConfList[current_list].packagesStatus,0,fileConfList[current_list].totalPackages);
    }

    if(current_page > fileConfList[current_list].totalPackages)
    {
        LOG_ERROR("FileId:%d CurrentPage%d>TotalPage%d",fileConfList[current_list].fileID,
                  current_page,fileConfList[current_list].totalPackages);
        FreeFileGroupSpace(current_list);
        return -4;
    }

    //if this package has exsist
    if(fileConfList[current_list].packagesStatus[current_page-1] == 1)
    {
        return -1;
    }

    gettimeofday(&tv1,NULL);
    fileConfList[current_list].timeVal = tv1.tv_sec;

    //print the message
    cout<<"FileID:"  <<fileConfList[current_list].fileID<<' '
        <<"Name:"<<fileConfList[current_list].name<<endl
        <<"List:"<<current_list<<' '
        <<"Total:"<<fileConfList[current_list].totalPackages<<' '
        <<"Current:"<<current_page<<' '
        <<"TimeOut:"<<FiletimeOut<<endl;

    int _pkg_len = get_pkg_length(content.c_str(), 4);
    //content length, 数字看收包格式协议
    int _str_len = _pkg_len - 1 - 4 - 2 - 2 - fileConfList[current_list].nameLenth - 4;

    ptr = &package_ptr[fileConfList[current_list].nameLenth + 8];  //文件内容
    //strlen(ptr)-1:without crc byte 减去４是因为需要去掉文件内容后面的４个长度的校验码
    fileConfList[current_list].content[current_page-1] = new char[_str_len + 1]; //+1, NULL bytes;
    memset(fileConfList[current_list].content[current_page-1], 0, _str_len + 1);
    strncpy(fileConfList[current_list].content[current_page-1], ptr, _str_len);
    //每一个分包收到后，分包的状态置１
    fileConfList[current_list].packagesStatus[current_page-1] = 1;

    //judge if the file is full
    for(unsigned int t=0;t<fileConfList[current_list].totalPackages;t++)
    {
        //说明还有某些分包没有收到，返回０，继续下一次分包接收和组包
        if(fileConfList[current_list].packagesStatus[t] == 0)
            return 0;
    }

    //Get the full file
    strcat(fileBuff,fileConfList[current_list].name);
    for(unsigned int z=0;z<fileConfList[current_list].totalPackages;z++)
    {
        strcat(fileBuff,fileConfList[current_list].content[z]);
    }

    //release the source
    FreeFileGroupSpace(current_list);

    return (int)fileConfList[current_list].fileID;
}

void FreeFileGroupSpace(int current_list_index)
{
    fileConfList[current_list_index].status = 0;
    fileConfList[current_list_index].timeVal = 0;

    for(unsigned int t=0;t<fileConfList[current_list_index].totalPackages;t++)
    {
        if(fileConfList[current_list_index].content[t] != NULL)
        {
            if(fileConfList[current_list_index].packagesStatus[t] == 1)
            {
                delete []fileConfList[current_list_index].content[t];
                fileConfList[current_list_index].content[t] = NULL;
            }
        }
    }
    if(fileConfList[current_list_index].content != NULL)
    {
        delete []fileConfList[current_list_index].content;
        fileConfList[current_list_index].content = NULL;
    }
    if(fileConfList[current_list_index].name != NULL)
    {
        delete []fileConfList[current_list_index].name;
        fileConfList[current_list_index].name = NULL;
    }
    if(fileConfList[current_list_index].packagesStatus != NULL)
    {
        delete []fileConfList[current_list_index].packagesStatus;
        fileConfList[current_list_index].packagesStatus = NULL;
    }

}

/********************String Group***********************/
typedef struct
{
    unsigned int status;  //current list has file,the status is 1,or is 0
    unsigned int stringID;
    unsigned int totalPackages;
    char *packagesStatus;
    char **content;
    int timeVal;
}StringConf;

/*******************String Content Form**********************
   4     1       2         2          x       1
 lenth  cmd  totalPage currentPage content   crc
******************************************************/
/****************************
Parameters:
    content:
    sendstring:
Return:
    <-1:error
    -1:string package is exsist
     0:string is not complete
     1:get a full string data
************************/

#define StringListNumber  50
#define StringTimeOutLimited    15
StringConf stringConfList[StringListNumber];
unsigned int timeOut = 0;
void FreeStringGroupSpace(int current_list_index);
int stringGroup(string &content,char *sendbuff)
{
    char *package_ptr = (char *)content.c_str()+5;
    int i=0,current_list=0;
    timeval tv1,tv2;

    char p[2] = {0};
    char *ptr = NULL;

    p[0] = package_ptr[0];
    p[1] = package_ptr[1];

    unsigned int total = strToInt(p,2);


    if(total == 1)
    {
        ptr = &package_ptr[4];
        memcpy(sendbuff,ptr,strlen(ptr)-4);
        return 1;
    }


    /****************Detect timeout********************/
    gettimeofday(&tv2,NULL);
    for(int j=0;j<StringListNumber;j++)
    {
        if(stringConfList[j].status == 1)
        {
            if((tv2.tv_sec - stringConfList[j].timeVal) > StringTimeOutLimited)
            {
                ++timeOut;
                LOG_WARN("String Timeout %d! stringID:%d,List:%d",
                         timeOut,
                         stringConfList[j].stringID,
                         j);
                cout<<"*******"<<j<<"******"<<stringConfList[j].stringID<<endl;
                FreeStringGroupSpace(j);
            }
        }
    }

    //Get string ID
    p[0] = package_ptr[19];
    p[1] = package_ptr[20];
    int id_H = strToInt(p,2);

    p[0] = package_ptr[21];
    p[1] = package_ptr[22];
    int id_L = strToInt(p,2);
    unsigned int packages_id = id_H*100 + id_L;

    p[0] = package_ptr[2];
    p[1] = package_ptr[3];
    unsigned int current_page = strToInt(p,2);

    if(current_page > total)
        return -4;

    /*******************Find the string list******************************/
    for(i=0;i<StringListNumber;i++)
    {
        if(stringConfList[i].stringID == packages_id)
        {
            //this items of string had already grouped
            if(stringConfList[i].status == 0)
            {
                return -2;
            }
            current_list = i;
            break;
        }
    }
    if(current_list == 0 && i==StringListNumber)
    {
        int j=0;
        for(j=0;j<StringListNumber;j++)
            if(stringConfList[j].status == 0)
            {
                current_list = j;
                stringConfList[current_list].stringID = packages_id;
                break;
            }
        if(j >= StringListNumber)
        {
            LOG_ERROR("j>StringListNumber return -3\n");
            return -3;
        }
    }
    /***************End of find list***********************/

    //judge if is the first packages of this items
    if(stringConfList[current_list].status==0)
    {
        stringConfList[current_list].totalPackages = total;
        stringConfList[current_list].status=1;
        stringConfList[current_list].packagesStatus = new char[stringConfList[current_list].totalPackages];
        stringConfList[current_list].content = new char *[stringConfList[current_list].totalPackages];
        for(unsigned int t=0;t<stringConfList[current_list].totalPackages;t++)
            stringConfList[current_list].content[t] = NULL;
        memset(stringConfList[current_list].packagesStatus,0,stringConfList[current_list].totalPackages);
    }else
    {
        //if this package has exsist
        if(stringConfList[current_list].packagesStatus[current_page-1] == 1)
            return -1;
    }

    if(current_page > stringConfList[current_list].totalPackages)
    {
        LOG_ERROR("ID:%d current %d > total %d  return -4",
                  packages_id,
                  current_page,
                  stringConfList[current_list].totalPackages);
        FreeStringGroupSpace(current_list);
        return -4;
    }
    gettimeofday(&tv1,NULL);
    stringConfList[current_list].timeVal = tv1.tv_sec;

    ptr = &package_ptr[31]; //4
    //strlen(ptr)-1:without crc byte
    stringConfList[current_list].content[current_page-1] = new char[strlen(ptr)-4];
    memset(stringConfList[current_list].content[current_page-1],0,strlen(ptr)-4);
    memcpy(stringConfList[current_list].content[current_page-1],ptr,strlen(ptr)-4);
    stringConfList[current_list].packagesStatus[current_page-1] = 1;

    cout<<"ID:"  <<stringConfList[current_list].stringID<<"  "
        <<"List:"<<current_list<<"  "
        <<"Total:"<<total<<"  "<<"Part:"<<current_page<<endl;
    cout<<"time out "<<timeOut<<endl;

    /*Detect if the string group is full*/
    for(unsigned int t=0;t<stringConfList[current_list].totalPackages;t++)
    {
        //说明还有某些分包没有收到，返回０，继续下一次分包接收和组包
        if(stringConfList[current_list].packagesStatus[t] == 0)
            return 0;
    }

    //Get the full file
    ptr = package_ptr+4; //cotent内容起点
    strncat(sendbuff,ptr,27); //27个字节是java生成的唯一标示一个字符串的时间戳
    for(unsigned int z=0;z<stringConfList[current_list].totalPackages;z++)
        strcat(sendbuff,stringConfList[current_list].content[z]);

    FreeStringGroupSpace(current_list);

    return 1;
}

void FreeStringGroupSpace(int current_list_index)
{
    stringConfList[current_list_index].status = 0;
    for(unsigned int t=0;t<stringConfList[current_list_index].totalPackages;t++)
    {
        if(stringConfList[current_list_index].content[t] != NULL)
        {
            if(stringConfList[current_list_index].packagesStatus[t] == 1)
            {
                delete []stringConfList[current_list_index].content[t];
                stringConfList[current_list_index].content[t] = NULL;
            }
        }
    }
    if(stringConfList[current_list_index].content != NULL)
    {
        delete []stringConfList[current_list_index].content;
        stringConfList[current_list_index].content = NULL;
    }
    if(stringConfList[current_list_index].packagesStatus != NULL)
    {
        delete []stringConfList[current_list_index].packagesStatus;
        stringConfList[current_list_index].packagesStatus = NULL;
    }
}

void *retryThread(void *param)
{
    string res;
    while(1)
    {
        pthread_mutex_lock(&_retry_mutex);

        if(!_retryCache.empty())
        {
            Mat src = _retryCache.front().clone();
            _retryCache.pop_front();
            pthread_mutex_unlock(&_retry_mutex);
            Mat mat;
            IplImage imgTemp = IplImage(src);
            IplImage *img;
            try
            {
                img = rotate_image(&imgTemp,2);
            }
            catch(cv::Exception)
            {

            }
            if(_QRDecode(img->imageData,img->width,img->height,res))
            {
                if(res.c_str()[4] == 'F')
                {
                    decoded_data_t data;
                    strcpy(data.data, res.c_str());
                    shmfifo_put(fifo_file, &data);
                }
                else if(res.c_str()[4] == '1'
                        || res.c_str()[4] == '2'
                        || res.c_str()[4] == '3')
                {
                    // pthread_mutex_lock(&_sendMutex);
                    // _sendCache.push_back(res);
                    // pthread_mutex_unlock(&_sendMutex);
                    decoded_data_t data;
                    strcpy(data.data, res.c_str());
                    shmfifo_put(fifo_str, &data);
                }
            }
            else
            {
                //cv::resize(src,mat,Size(src.cols/2,src.rows/2),0,0,INTER_CUBIC);
                Mat mat;
                cv::resize(src,mat,Size(src.cols/2,src.rows/2),0,0,INTER_AREA);
                IplImage img1(mat);

                if(_QRDecode(img1.imageData,img1.width,img1.height,res))
                {
                    if(res.c_str()[4] == 'F')
                    {
                        // pthread_mutex_lock(&_fileSendMutex);
                        // _fileSendCache.push_back(res);
                        // pthread_mutex_unlock(&_fileSendMutex);
                        decoded_data_t data;
                        strcpy(data.data, res.c_str());
                        shmfifo_put(fifo_file, &data);
                    }
                    else if(res.c_str()[4] == '1'
                            || res.c_str()[4] == '2'
                            || res.c_str()[4] == '3')
                    {
                        // pthread_mutex_lock(&_sendMutex);
                        // _sendCache.push_back(res);
                        // pthread_mutex_unlock(&_sendMutex);
                        decoded_data_t data;
                        strcpy(data.data, res.c_str());
                        shmfifo_put(fifo_str, &data);
                    }
                }

            }
            cvReleaseImage(&img);
        }
        else
        {
            pthread_mutex_unlock(&_retry_mutex);
            usleep(_config.interval*20*1000);
            continue;
        }
    }
    return NULL;
}

#define StringMaxSize 4*1024
void *sendThread(void *param)
{
    shmfifo_t *fifo = fifo_str;
    char sendBuff[StringMaxSize] = {0};   //the max size string data
    decoded_data_t data;
    string res;

    printf("string shmid = %d\n", fifo->shmid);
    while(1)
    {
        if (shmget(fifo->p_shm->key, 0, 0) == -1)
        {
            break;
        }

        sem_p(fifo->sem_mutex);
        int count = sem_getval(fifo->sem_empty);
        if (count > 0)
        {
            sem_v(fifo->sem_mutex);

            //block util data available
            shmfifo_get(fifo, &data);

            res = data.data;
        }
        else
        {
            sem_v(fifo->sem_mutex);
            usleep(1000);
            continue;
        }

        int result = stringGroup(res,sendBuff+5);
        if(result == 1)
        {
            int stringLenth = strlen(sendBuff+5)+1;
            sprintf(sendBuff, "%04x", stringLenth);
            sendBuff[4] = res.c_str()[4];
            char serialNumebr[32] = {0};
            memcpy(serialNumebr,&sendBuff[5],27);
            cout<<serialNumebr<<endl;
            if(!sendDataSocket(sendBuff))
            {
                sockfd = initSocket();
            }
            memset(sendBuff,0,sizeof(sendBuff));
        }
    }

    shmfifo_destroy(fifo);

    return NULL;
}

#define FileMaxSize 100*1024
void *sendFileThread(void *param)
{
    shmfifo_t *fifo = fifo_file;
    char sendFileString[FileMaxSize] = {0};
    decoded_data_t data;
    string fileRes;

    while(1)
    {
        //share memory has been released
        if (shmget(fifo->p_shm->key, 0, 0) == -1)
        {
            break;
        }

        sem_p(fifo->sem_mutex);
        int count = sem_getval(fifo->sem_empty);
        if (count > 0)
        {
            sem_v(fifo->sem_mutex);

            //block util data available, so there is no neede to sleep
            shmfifo_get(fifo, &data);

            fileRes = data.data;
        }
        else
        {
            sem_v(fifo->sem_mutex);
            usleep(1000);
            continue;
        }

        //tips:| 4 | 1 | 4 | -> pkg_len, flag, file_id
        int res = fileGroup(fileRes,sendFileString+9);
        if(res > 0)
        {
            int fileLenth = strlen(sendFileString+9)+4;
            sprintf(sendFileString, "%04x", fileLenth);
            sendFileString[4] = 'F';
            char idBuff[4]={0};
            sprintf(idBuff, "%04x", res);
            memcpy(sendFileString+5,idBuff,4);

            // add file name
            char buff[128] = {0};
            int cnt = 0;
            for(cnt=0;cnt<128;cnt++)
            {
                if(sendFileString[cnt] == '.')
                    break;
            }

            memcpy(buff,sendFileString,cnt+4);
            cout<<"Send:"<<buff<<endl;
            LOG_DEBUG("%s\t ID:%d",buff,res);

            writeToFile(sendFileString);

            if(!sendDataSocket(buff))
            {
                LOG_ERROR("file send error!");
                sockfd = initSocket();
            }

            memset(sendFileString,0,sizeof(sendFileString));
        }
    }

    shmfifo_destroy(fifo);

    return NULL;
}

int writeToFile(const char *buf)
{
    if (buf == NULL || strlen(buf) == 0)
    {
        return 0;
    }

    FILE *fp = NULL;
    char filename[256] = "src/";
    int path_length = strlen(filename);

    int cnt = 0;
    for (cnt = 0; cnt < 256; ++cnt)
    {
        if (buf[cnt] == '.')
        {
            break;
        }
    }

    memcpy(filename + path_length, buf + 9, cnt - 9 + 4);
    cout << "filename:" << filename << endl;

    fp = fopen(filename, "w");
    if (fp == NULL)
    {
        LOG_ERROR("new file to be write error, filename = %s\n", filename);
        return -1;
    }

    const char *ptr = buf + cnt + 4;
    static int total = strlen(ptr);
    static int writed;
    writed = fwrite(ptr, sizeof(char), total, fp);

    static int status;
    if (writed == total)
    {
        status = 0;
        LOG_DEBUG("write to file success, total bytes = %d, writed = %d\n!",
                  total, writed);
    }
    else
    {
        status = -1;
        LOG_DEBUG("write to file failed, total bytes = %d, writed = %d\n!",
                  total,
                  writed);
    }

    fflush(fp);

    return status;
}

void checkFile(string path)
{
    if(access(path.c_str(),F_OK|R_OK)!=0)
    {
        int fd = open(path.c_str(),O_CREAT,O_RDWR);
        close(fd);
    }
}

bool LogInit(LogLevel l)
{
    //variable to construct the file name which including time stamp
    char makeFileName[64];

    time_t tmp = time(NULL);
    tm* tmpTime = localtime(&tmp);

    //detail filename, accurate to seconds
    snprintf(makeFileName, 64,"%d%02d%02d_%02d%02d%02d",
             tmpTime->tm_year+1900,
             tmpTime->tm_mon+1,
             tmpTime->tm_mday,
             tmpTime->tm_hour,
             tmpTime->tm_min,
             tmpTime->tm_sec);

    string Path = "logs_decode/LOG_";
    Path.append(makeFileName);

    return log_init(l, Path.c_str(), "./");
}

//每个分包前len位表示接下来的字节数
//eg: "abcd" -> 10 * 16*16*16 + 11 * 16*16 + 12 * 16 + 13
//已跑过单元测试:
/*
  assert(get_pkg_length("aBCd", 4) == 43981);
  assert(get_pkg_length("ab", 2) == 171);
  assert(get_pkg_length("e2fDF", 2) != 929759);
  */
int get_pkg_length(const char *head, const int len)
{
    if (len <= 0)
    {
        return 0;
    }

    char *buf = (char*)calloc(len + 1, sizeof(char)); //+1, NULL char. for log debug.
    strncpy(buf, head, len);

    //tolower
    char ch;
    for (int k = 0; k < len; ++k)
    {
        ch = buf[k];
        buf[k] = ((ch >= 'A') && (ch <= 'Z')) ? (ch | 0x20) : ch;
    }

    for (int i = 0; i < len; ++i)
    {
        if (buf[i] > 'f')
        {
            LOG_ERROR("package header error:%s\n", buf);

            free(buf);
            buf = NULL;

            return -1;
        }
    }

    int i, j;
    int sum = 0;
    int num;
    for (i = len - 1, j = 0; i >=0; --i, ++j) //从后往前计算
    {
        if (buf[i] >= 'a' && buf[i] <= 'f')
        {
            num = buf[i] - 'a' + 10;
        }
        else
        {
            num = buf[i] - '0';
        }

        sum += num * (int)pow(16, j);
    }

    free(buf);
    buf = NULL;

    return sum;
}

//以前的实现标记文件分片的内容的开始位置为: | some contet .. finame.txt | content | four byte checksum |
static const char *content_location_of_pkg(const char *buf)
{
    const char *ptr = NULL;

    ptr = strcasestr(buf, ".txt");
    if (ptr != NULL)
    {
        ptr += 4;
    }

    return ptr;
}

//对收到的分包进行校验，收到的不全就丢掉(用于对两个decode后数据整合前进行过滤)
bool received_file_pkg_is_complete(const string &buf)
{
    if (buf.size() <= 0)
    {
        return false;
    }

    const char *str = buf.c_str();
    //the first 4 bytes store the len of the rest this package.
    int pkg_len = get_pkg_length(str, 4);
    //收到的包长度不一致，不用做crc校验，判断为丢包, +4: 跳过头四个字节, 头四个字节标记此包后的长度
    if (strlen(str + 4) != pkg_len)
    {
        return false;
    }

    const char *content_head = content_location_of_pkg(str);

    char checksum[5];
    int _len = strlen(str);
    strncpy(checksum, str + _len - 4, 4);
    checksum[4] = '\0';

    char *content = strdup(str);
    _len = strlen(content);
    content[_len - 4] = '\0';

    int crc = verifystr(content, strlen(content));
    char crcStr[5];
    sprintf(crcStr, "%04x", crc);
    crcStr[4] = '\0';

    bool is_complete = strcasecmp(checksum, crcStr) == 0 ? true : false;

    return is_complete;
}
