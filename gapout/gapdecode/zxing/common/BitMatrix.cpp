// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  Copyright 2010 ZXing authors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "BitMatrix.h"
#include "IllegalArgumentException.h"

#include <iostream>
#include <sstream>
#include <string>

using std::ostream;
using std::ostringstream;

using zxing::BitMatrix;
using zxing::BitArray;
using zxing::Ref;


/*
  size_t wordsForSize(size_t width,
                      size_t height,
                      unsigned int bitsPerWord,
                      unsigned int logBits) {
    size_t bits = width * height;
    int arraySize = (bits + bitsPerWord - 1) >> logBits;
    return arraySize;
  }*/
size_t BitMatrix::wordsForSize(size_t width,
                               size_t height)
{
    int rowSize = width>>5;
    if((width & 0x1f) != 0)
    {
        rowSize++;
    }
    this->rowSize_ = rowSize;
    int arraySize = rowSize * height ;
    return arraySize;
}


BitMatrix::BitMatrix(size_t dimension) :
    width_(dimension), height_(dimension), words_(0), bits_(NULL)
{
    words_ = wordsForSize(width_, height_);
    bits_ = new int[words_];
    clear();
}

BitMatrix::BitMatrix(size_t width, size_t height) :
    width_(width), height_(height), words_(0), bits_(NULL)
{
    words_ = wordsForSize(width_, height_);
    bits_ = new int[words_];
    clear();
}

BitMatrix::~BitMatrix()
{
    delete[] bits_;
}

void BitMatrix::flip(size_t x, size_t y)
{
    size_t offset = (x>>5) + rowSize_ * y;
    bits_[offset] ^= 1 << (x & 0x1f);
}

void BitMatrix::clear()
{
    std::fill(bits_, bits_+words_, 0);
}

void BitMatrix::setRegion(size_t left, size_t top, size_t width, size_t height)
{
    if ((long)top < 0 || (long)left < 0)
    {
        throw IllegalArgumentException("topI and leftJ must be nonnegative");
    }
    if (height < 1 || width < 1)
    {
        throw IllegalArgumentException("height and width must be at least 1");
    }
    size_t right = left + width;
    size_t bottom = top + height;
    if (right > width_ || bottom > height_)
    {
        throw IllegalArgumentException("top + height and left + width must be <= matrix dimension");
    }
    for (size_t y = top; y < bottom; y++)
    {
        int offset = rowSize_ * y;
        for (size_t x = left; x < right; x++)
        {

            bits_[offset +(x>>5)] |= 1 << (x & 0x1f);
        }
    }
}

Ref<BitArray> BitMatrix::getRow(int y, Ref<BitArray> row)
{
    if (row.empty() || row->getSize() < width_)
    {
        row = new BitArray(width_);
    }
    size_t offset = y * rowSize_;
    for (size_t x = 0; x < rowSize_; x++)
    {
        row->setBulk(x << 5, bits_[offset + x]);
    }
    return row;
}

size_t BitMatrix::getWidth() const
{
    return width_;
}

size_t BitMatrix::getHeight() const
{
    return height_;
}

size_t BitMatrix::getDimension() const
{
    if(width_!=height_)
        throw IllegalArgumentException("width not equals height");
    return width_;
}

int* BitMatrix::getBits() const
{
    return bits_;
}
namespace zxing
{
ostream& operator<<(ostream &out, const BitMatrix &bm)
{
    for (size_t y = 0; y < bm.height_; y++)
    {
        for (size_t x = 0; x < bm.width_; x++)
        {
            out << (bm.get(x, y) ? "X " : "  ");
        }
        out << "\n";
    }
    return out;
}
}

const char* BitMatrix::description()
{
    ostringstream out;
    out << *this;
    return out.str().c_str();
}
