// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  GlobalHistogramBinarizer.cpp
 *  zxing
 *
 *  Copyright 2010 ZXing authors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "GlobalHistogramBinarizer.h"
#include"IllegalArgumentException.h"
#include "Array.h"

namespace zxing
{
using namespace std;

const int LUMINANCE_BITS = 5;
const int LUMINANCE_SHIFT = 8 - LUMINANCE_BITS;
const int LUMINANCE_BUCKETS = 1 << LUMINANCE_BITS;

GlobalHistogramBinarizer::GlobalHistogramBinarizer(Ref<LuminanceSource> source) :
    Binarizer(source), cached_matrix_(NULL), cached_row_(NULL), cached_row_num_(-1)
{

}

GlobalHistogramBinarizer::~GlobalHistogramBinarizer()
{
}

Ref<BitMatrix> GlobalHistogramBinarizer::getBlackMatrix()
{
    /*if (cached_matrix_ != NULL) {
      return cached_matrix_;
    }*/

    // Faster than working with the reference
    LuminanceSource& source = *getLuminanceSource();
    char* localLuminances;

    int width = source.getWidth();
    int height = source.getHeight();

    Ref<BitMatrix> matrix(new BitMatrix(width, height));

    //initArrays(width);
    char luminances[width];
    vector<int> buckets(LUMINANCE_BUCKETS, 0);


    vector<int>localBuckets = buckets;

    // Quickly calculates the histogram by sampling four rows from the image.
    // This proved to be more robust on the blackbox tests than sampling a
    // diagonal as we used to do.

    //unsigned char* row = &ref[0];
    for (int y = 1; y < 5; y++)
    {
        int row = height * y / 5;
        localLuminances = source.getRow(row,luminances,width);
        int right = (width << 2) / 5;
        //row = source.getRow(rownum, row);
        for (int x = width / 5; x < right; x++)
        {
            int pixel=localLuminances[x]&0xff;
            localBuckets[pixel >> LUMINANCE_SHIFT]++;
        }
    }

    int blackPoint = estimate(localBuckets);

    localLuminances = source.getMatrix();
    //BitMatrix& matrix = *matrix_ref;
    for (int y = 0; y < height; y++)
    {
        int offset=y*width;
        //row = source.getRow(y, row);
        for (int x = 0; x < width; x++)
        {
            int pixel = localLuminances[offset+x]&0xff;
            if(pixel<blackPoint)
            {
                matrix->set(x,y);
            }
        }
    }

    //cached_matrix_ = matrix_ref;
    // delete [] row;
    return matrix;
}

Ref<BitArray> GlobalHistogramBinarizer::getBlackRow(int y, Ref<BitArray> row)
{
    /*if (y == cached_row_num_) {
      if (cached_row_ != NULL) {
        return cached_row_;
      } else {
        throw IllegalArgumentException("Too little dynamic range in luminance");
      }
    }*/


    //vector<int> histogram(LUMINANCE_BUCKETS, 0);
    LuminanceSource& source = *getLuminanceSource();
    int width = source.getWidth();
    if (row == NULL || static_cast<int>(row->getSize()) < width)
    {
        row = new BitArray(width);
    }
    else
    {
        row->clear();
    }

    //initArray
    char luminances[width];
    vector<int> buckets(LUMINANCE_BUCKETS, 0);


    vector<int> localBuckets=buckets;


    //TODO(flyashi): cache this instead of allocating and deleting per row
    //unsigned char* row_pixels = NULL;
    try
    {
        char* localLuminances=source.getRow(y,luminances,width);
        for (int x = 0; x < width; x++)
        {
            int pixel=localLuminances[x]&0xff;
            localBuckets[pixel >> LUMINANCE_SHIFT]++;
        }
        int blackPoint = estimate(localBuckets);

        //BitArray& array = *row;
        int left = localLuminances[0];
        int center = localLuminances[1];
        for (int x = 1; x < width - 1; x++)
        {
            int right = localLuminances[x + 1]&0xff;
            // A simple -1 4 -1 box filter with a weight of 2.
            int luminance = ((center << 2) - left - right) >> 1;
            if (luminance < blackPoint)
            {
                //array.set(x);
                row->set(x);
            }
            left = center;
            center = right;
        }

        return row;
    }
    catch (IllegalArgumentException const& iae)
    {
        // Cache the fact that this row failed.
        throw iae;
    }
}


int GlobalHistogramBinarizer::estimate(vector<int> &buckets)
{
    int numBuckets = buckets.size();
    int maxBucketCount = 0;

    // Find tallest peak in histogram
    int firstPeak = 0;
    int firstPeakSize = 0;
    for (int i = 0; i < numBuckets; i++)
    {
        if (buckets[i] > firstPeakSize)
        {
            firstPeak = i;
            firstPeakSize = buckets[i];
        }
        if (buckets[i] > maxBucketCount)
        {
            maxBucketCount = buckets[i];
        }
    }

    // Find second-tallest peak -- well, another peak that is tall and not
    // so close to the first one
    int secondPeak = 0;
    int secondPeakScore = 0;
    for (int i = 0; i < numBuckets; i++)
    {
        int distanceToBiggest = i - firstPeak;
        // Encourage more distant second peaks by multiplying by square of distance
        int score = buckets[i] * distanceToBiggest * distanceToBiggest;
        if (score > secondPeakScore)
        {
            secondPeak = i;
            secondPeakScore = score;
        }
    }

    // Put firstPeak first
    if (firstPeak > secondPeak)
    {
        int temp = firstPeak;
        firstPeak = secondPeak;
        secondPeak = temp;
    }

    // Kind of arbitrary; if the two peaks are very close, then we figure there is
    // so little dynamic range in the image, that discriminating black and white
    // is too error-prone.
    // Decoding the image/line is either pointless, or may in some cases lead to
    // a false positive for 1D formats, which are relatively lenient.
    // We arbitrarily say "close" is
    // "<= 1/16 of the total histogram buckets apart"
    if (secondPeak - firstPeak <= numBuckets >> 4)
    {
        throw IllegalArgumentException("Too little dynamic range in luminance");
    }

    // Find a valley between them that is low and closer to the white peak
    int bestValley = secondPeak - 1;
    int bestValleyScore = -1;
    for (int i = secondPeak - 1; i > firstPeak; i--)
    {
        int fromFirst = i - firstPeak;
        // Favor a "valley" that is not too close to either peak -- especially not
        // the black peak -- and that has a low value of course
        int score = fromFirst * fromFirst * (secondPeak - i) *
                    (maxBucketCount - buckets[i]);
        if (score > bestValleyScore)
        {
            bestValley = i;
            bestValleyScore = score;
        }
    }

    return bestValley << LUMINANCE_SHIFT;
}

Ref<Binarizer> GlobalHistogramBinarizer::createBinarizer(Ref<LuminanceSource> source)
{
    return Ref<Binarizer> (new GlobalHistogramBinarizer(source));
}

} // namespace zxing
