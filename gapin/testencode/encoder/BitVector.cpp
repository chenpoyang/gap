#include "BitVector.h"
namespace zhixin {
BitVector::BitVector() {
    sizeInBits = 0;
    array.resize(DEFAULT_SIZE_IN_BYTES);
}

BitVector::~BitVector() {
    //dtor
    //delete &array;
}

vector<char> & BitVector::Array() {
    return array;
}

int BitVector::size() {
    return sizeInBits;
}
int BitVector::sizeInBytes() {
    return (sizeInBits + 7) >> 3;
}
int BitVector::at(int index) {
    if (index < 0 || index >= sizeInBits) {
        throw Exception("Bad index: " + index);
    }
    int value_Renamed = array[index >> 3] & 0xff;
    return (value_Renamed >> (7 - (index & 0x7))) & 1;
}
void  BitVector::appendBit(int bit) {
    if (!(bit == 0 || bit == 1)) {
        throw Exception("Bad bit");
    }
    int numBitsInLastByte = sizeInBits & 0x7;
    // We'll expand array if we don't have bits in the last byte.
    if (numBitsInLastByte == 0) {
        appendByte(0);
        sizeInBits -= 8;
    }
    // Modify the last byte.
    array[sizeInBits >> 3] |= (char) ((bit << (7 - numBitsInLastByte)));
    ++sizeInBits;
}
void  BitVector::appendBits(int value_Renamed, int numBits) {
    if (numBits < 0 || numBits > 32) {
        throw Exception("Num bits must be between 0 and 32");
    }
    int numBitsLeft = numBits;
    while (numBitsLeft > 0) {
        // Optimization for byte-oriented appending.
        if ((sizeInBits & 0x7) == 0 && numBitsLeft >= 8) {
            int newByte = (value_Renamed >> (numBitsLeft - 8)) & 0xff;
            appendByte(newByte);
            numBitsLeft -= 8;
        } else {
            int bit = (value_Renamed >> (numBitsLeft - 1)) & 1;
            appendBit(bit);
            --numBitsLeft;
        }
    }
}

void BitVector::appendBitVector(Ref<BitVector> bits) {
    int size = bits->size();
    for (int i = 0; i < size; ++i) {
        appendBit(bits->at(i));
    }
}
void  BitVector::xore(Ref<BitVector> other) {
    if (sizeInBits != other->size()) {
        throw Exception("BitVector sizes don't match");
    }
    int sizeInBytes = (sizeInBits + 7) >> 3;
    for (int i = 0; i < sizeInBytes; ++i) {
        // The last byte could be incomplete (i.e. not have 8 bits in
        // it) but there is no problem since 0 XOR 0 == 0.
        array[i] ^= other->Array()[i];
    }
}
string BitVector::ToString() {
    string result;
    for(int i=0; i<sizeInBits; i++) {
        result += "";
    }
    for (int i = 0; i < sizeInBits; ++i) {
        if (at(i) == 0) {
            result+=('0');
        } else if (at(i) == 1) {
            result+=('1');
        } else {
            throw Exception("Byte isn't 0 or 1");
        }
    }
    return result;
}
void  BitVector::appendByte(int value_Renamed) {
    int len = array.size();
    if ((sizeInBits >> 3) == len) {
        //char newArray[(sizeof(array) << 1)];
        // Redivivus.in Java to c# Porting update
        // 30/01/2010
        // added namespace system
        //System.Array.Copy(array, 0, newArray, 0, sizeof(array));
        //array = newArray;
        array.resize(array.size() << 1);
    }
    array[sizeInBits >> 3] = (char) value_Renamed;
    sizeInBits += 8;
}
}
