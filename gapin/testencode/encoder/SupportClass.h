#ifndef SUPPORTCLASS_H
#define SUPPORTCLASS_H

#include <string>
#include <vector>
#include "reedsolomon/Array.h"
using namespace std;

namespace zhixin {
namespace qrcode {
class SupportClass {
public:
    SupportClass();
    virtual ~SupportClass();

    /*        static unsigned char* ToByteArray(char sbyteArray[]);
            static char* ToByteArray(string sourceString);
            //static byte[] ToByteArray(System.Object[] tempObjectArray);
            static int URShift(int number, long bits);*/
    static int URShift(int number, int bits);
    /*       static long URShift(long number, int bits);
           static long URShift(long number, long bits);
           static long Identity(long literal);
           static unsigned long Identity(unsigned long literal);
           static float Identity(float literal);

           static void GetCharsFromString(string sourceString, int sourceStart, int sourceEnd, char destinationArray[], int destinationStart);
           //static void SetCapacity(System.Collections.ArrayList vector, int newCapacity);*/
    static int Identity(int literal);
    static ArrayRef<char> ToSByteArray(ArrayRef<unsigned char> byteArray);
protected:
private:
};
}
}
#endif // SUPPORTCLASS_H
