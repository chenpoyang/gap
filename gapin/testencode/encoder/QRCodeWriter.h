#ifndef QRCODEWRITER_H
#define QRCODEWRITER_H

using namespace std;
#include <string>
//#include "BarcodeFormat.h"

#include "Counted.h"
#include "QRCode.h"
#include "Encoder.h"
#include "SupportClass.h"
#include "ErrorCorrectionLevel.h"
namespace zhixin {
namespace qrcode {
class QRCodeWriter {
public:
    QRCodeWriter();
    //Ref<ByteMatrix> encode(string contents, Ref<BarcodeFormat> format, int width, int height);
    Ref<ByteMatrix> encode(string contents, /*BarcodeFormat format,*/ int width, int height, string hints);
    Ref<ByteMatrix> encode(char* contents, int length,int width, int height, string hints);

    virtual ~QRCodeWriter();


    Ref<ByteMatrix> renderResult(Ref<QRCode> code, int width, int height);
    void setRowColor(char row[], int size, char value_Renamed);

protected:
private:
    static  const int QUIET_ZONE_SIZE = 4;
};
}
}
#endif // QRCODEWRITER_H
