// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
#ifndef __MODE_H__
#define __MODE_H__

/*
 *  Mode.h
 *  zxing
 *
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "Version.h"

#include <sstream>
#include "Counted.h"
#include <string>
using namespace std;

namespace zhixin {
namespace qrcode {

class Mode : public Counted{
private:
  int characterCountBitsForVersions0To9_;
  int characterCountBitsForVersions10To26_;
  int characterCountBitsForVersions27AndHigher_;
  int bits_;
  std::string name_;

Mode(int cbv0_9, int cbv10_26, int cbv27, int bits, char const* name);

public:
    int Bits();
    string Name();
    Mode();
   // virtual ~Mode();
  static Ref<Mode> TERMINATOR;
  static Ref<Mode> NUMERIC;
  static Ref<Mode> ALPHANUMERIC;
  static Ref<Mode> STRUCTURED_APPEND;
  static Ref<Mode> BYTE;
  static Ref<Mode> ECI;
  static Ref<Mode> KANJI;
  static Ref<Mode> FNC1_FIRST_POSITION;
  static Ref<Mode> FNC1_SECOND_POSITION;
  static Ref<Mode> HANZI;

  static Ref<Mode> forBits(int bits);
  int getCharacterCountBits(Version* version);
 // bool operator == (const Mode& s);
    bool isEquals(Ref<Mode> mode);
};
}
}

#endif // __MODE_H__
