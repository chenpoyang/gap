#ifndef BLOCKPAIR_H
#define BLOCKPAIR_H

#include "Counted.h"
#include "ByteArray.h"
namespace zhixin{
    namespace qrcode{
class BlockPair: public Counted
{
    public:
        BlockPair();
        BlockPair(Ref<ByteArray> data, Ref<ByteArray> errorCorrection);
        virtual ~BlockPair();
        Ref<ByteArray> dataBytes;
        Ref<ByteArray> errorCorrectionBytes;


    protected:
    private:
};
    }}
#endif // BLOCKPAIR_H
