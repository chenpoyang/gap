/*
 *  Version.cpp
 *  zxing
 *
 *  Created by Christian Brunschen on 14/05/2008.
 *  Copyright 2008 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <limits>
#include <iostream>
#include <cstdarg>

#include "Version.h"

namespace zhixin {
namespace qrcode {
using namespace std;
// ecb class
ECB::ECB(int count, int dataCodewords) :
    count(count), dataCodewords(dataCodewords) {
}

int ECB::Count() {
    return count;
}

int ECB::DataCodewords() {
    return dataCodewords;
}


// ecblocks class
ECBlocks::ECBlocks(int ecCodewordsPerBlock, Ref<ECB> ecBlocks) :
    ecCodewordsPerBlock(ecCodewordsPerBlock), ecBlocks(1, ecBlocks) {
}

ECBlocks::ECBlocks(int ecCodewordsPerBlock, Ref<ECB> ecBlocks1, Ref<ECB> ecBlocks2) :
    ecCodewordsPerBlock(ecCodewordsPerBlock), ecBlocks(1, ecBlocks1) {
    ecBlocks.push_back(ecBlocks2);
}
int ECBlocks::NumBlocks() {
    int total = 0;
    for (int i = 0; i < (int)(ecBlocks.size()); i++) {
        total += ecBlocks[i]->Count();
    }
    return total;
}
int ECBlocks::ECCodewordsPerBlock() {
    return ecCodewordsPerBlock;
}
int ECBlocks::TotalECCodewords() {
    return ecCodewordsPerBlock * NumBlocks();
}
std::vector<Ref<ECB> >& ECBlocks::getECBlocks() {
    return ecBlocks;
}

ECBlocks::~ECBlocks() {
    /*for (size_t i = 0; i < ecBlocks.size(); i++) {
      delete ecBlocks[i];
    }*/
}


// Version Class

unsigned int Version::VERSION_DECODE_INFO[] = { 0x07C94, 0x085BC, 0x09A99, 0x0A4D3, 0x0BBF6, 0x0C762, 0x0D847, 0x0E60D,
        0x0F928, 0x10B78, 0x1145D, 0x12A17, 0x13532, 0x149A6, 0x15683, 0x168C9, 0x177EC, 0x18EC4, 0x191E1, 0x1AFAB,
        0x1B08E, 0x1CC1A, 0x1D33F, 0x1ED75, 0x1F250, 0x209D5, 0x216F0, 0x228BA, 0x2379F, 0x24B0B, 0x2542E, 0x26A64,
        0x27541, 0x28C69
                                              };
int Version::N_VERSION_DECODE_INFOS = 34;
vector<Ref<Version> > Version::VERSIONS;
static int N_VERSIONS = Version::buildVersions();

int Version::VersionNumber() {
    return versionNumber;
}

vector<int> &Version::AlignmentPatternCenters() {
    return alignmentPatternCenters;
}

int Version::TotalCodewords() {
    return totalCodewords;
}

int Version::DimensionForVersion() {
    return 17 + 4 * versionNumber;
}

Ref<ECBlocks>& Version::getECBlocksForLevel(Ref<ErrorCorrectionLevel> ecLevel) {
    return ecBlocks[ecLevel->ordinal()];
}

Ref<Version> Version::getProvisionalVersionForDimension(int dimension) {
    if (dimension % 4 != 1) {
        throw Exception("Dimension must be 1 mod 4");
    }
    return Version::getVersionForNumber((dimension - 17) >> 2);
}

Ref<Version> Version::getVersionForNumber(int versionNumber) {
    if (versionNumber < 1 || versionNumber > N_VERSIONS) {
        throw Exception("versionNumber must be between 1 and 40");
    }

    return VERSIONS[versionNumber - 1];
}

Version::Version(int versionNumber, vector<int> alignmentPatternCenters, Ref<ECBlocks> ecBlocks1, Ref<ECBlocks> ecBlocks2,
                 Ref<ECBlocks> ecBlocks3, Ref<ECBlocks> ecBlocks4) :
    versionNumber(versionNumber), alignmentPatternCenters(alignmentPatternCenters), ecBlocks(4), totalCodewords(0) {
    ecBlocks[0] = ecBlocks1;
    ecBlocks[1] = ecBlocks2;
    ecBlocks[2] = ecBlocks3;
    ecBlocks[3] = ecBlocks4;

    int total = 0;
    int ecCodewords = ecBlocks1->ECCodewordsPerBlock();
    vector<Ref<ECB> > &ecbArray = ecBlocks1->getECBlocks();
    for (size_t i = 0; i < ecbArray.size(); i++) {
        Ref<ECB> ecBlock = ecbArray[i];
        total += ecBlock->Count() * (ecBlock->DataCodewords() + ecCodewords);
    }
    totalCodewords= total;
}

Version::~Version() {
    /*  delete &alignmentPatternCenters;
      for (size_t i = 0; i < ecBlocks.size(); i++) {
        delete ecBlocks[i];
      }*/
}



Ref<Version> Version::decodeVersionInformation(unsigned int versionBits) {
    int bestDifference = numeric_limits<int>::max();
    size_t bestVersion = 0;
    for (int i = 0; i < N_VERSION_DECODE_INFOS; i++) {
        unsigned targetVersion = VERSION_DECODE_INFO[i];
        // Do the version info bits match exactly? done.
        if (targetVersion == versionBits) {
            return getVersionForNumber(i + 7);
        }
        // Otherwise see if this is the closest to a real version info bit
        // string we have seen so far
        int bitsDifference = FormatInformation::numBitsDiffering(versionBits, targetVersion);
        if (bitsDifference < bestDifference) {
            bestVersion = i + 7;
            bestDifference = bitsDifference;
        }
    }
    // We can tolerate up to 3 bits of error since no two version info codewords will
    // differ in less than 4 bits.
    if (bestDifference <= 3) {
        return getVersionForNumber(bestVersion);
    }
    // If we didn't find a close enough match, fail
    return Ref<Version>(NULL);
}

Ref<BitMatrix> Version::buildFunctionPattern() {
    int dimension = DimensionForVersion();
    Ref<BitMatrix> functionPattern(new BitMatrix(dimension));


    // Top left finder pattern + separator + format
    functionPattern->setRegion(0, 0, 9, 9);
    // Top right finder pattern + separator + format
    functionPattern->setRegion(dimension - 8, 0, 8, 9);
    // Bottom left finder pattern + separator + format
    functionPattern->setRegion(0, dimension - 8, 9, 8);


    // Alignment patterns
    size_t max = alignmentPatternCenters.size();
    for (size_t x = 0; x < max; x++) {
        int i = alignmentPatternCenters[x] - 2;
        for (size_t y = 0; y < max; y++) {
            if ((x == 0 && (y == 0 || y == max - 1)) || (x == max - 1 && y == 0)) {
                // No alignment patterns near the three finder patterns
                continue;
            }
            functionPattern->setRegion(alignmentPatternCenters[y] - 2, i, 5, 5);
        }
    }

    // Vertical timing pattern
    functionPattern->setRegion(6, 9, 1, dimension - 17);
    // Horizontal timing pattern
    functionPattern->setRegion(9, 6, dimension - 17, 1);

    if (versionNumber > 6) {
        // Version info, top right
        functionPattern->setRegion(dimension - 11, 0, 3, 6);
        // Version info, bottom left
        functionPattern->setRegion(0, dimension - 11, 6, 3);
    }
    return functionPattern;
}

static vector<int> intArray(size_t n...) {
    va_list ap;
    va_start(ap, n);
    //vector<int> *result = new vector<int>(n);
    vector<int> result(n);
    for (size_t i = 0; i < n; i++) {
        (result)[i] = va_arg(ap, int);
    }
    va_end(ap);
    return result;
}

int Version::buildVersions() {
    VERSIONS.push_back(Ref<Version>(new Version(1, intArray(0),
                                    Ref<ECBlocks>( new ECBlocks(7, Ref<ECB>(new ECB(1, 19)))),
                                    Ref<ECBlocks>( new ECBlocks(10, Ref<ECB>(new ECB(1, 16)))),
                                    Ref<ECBlocks>( new ECBlocks(13, Ref<ECB>(new ECB(1, 13)))),
                                    Ref<ECBlocks>( new ECBlocks(17, Ref<ECB>(new ECB(1, 9)))))));
    VERSIONS.push_back(Ref<Version>(new Version(2, intArray(2, 6, 18),
                                    Ref<ECBlocks>( new ECBlocks(10, Ref<ECB>(new ECB(1, 34)))),
                                    Ref<ECBlocks>( new ECBlocks(16, Ref<ECB>(new ECB(1, 28)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(1, 22)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(1, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(3, intArray(2, 6, 22),
                                    Ref<ECBlocks>( new ECBlocks(15, Ref<ECB>(new ECB(1, 55)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(1, 44)))),
                                    Ref<ECBlocks>( new ECBlocks(18, Ref<ECB>(new ECB(2, 17)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(2, 13)))))));
    VERSIONS.push_back(Ref<Version>(new Version(4, intArray(2, 6, 26),
                                    Ref<ECBlocks>( new ECBlocks(20, Ref<ECB>(new ECB(1, 80)))),
                                    Ref<ECBlocks>( new ECBlocks(18, Ref<ECB>(new ECB(2, 32)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(2, 24)))),
                                    Ref<ECBlocks>( new ECBlocks(16, Ref<ECB>(new ECB(4, 9)))))));
    VERSIONS.push_back(Ref<Version>(new Version(5, intArray(2, 6, 30),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(1, 108)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(2, 43)))),
                                    Ref<ECBlocks>( new ECBlocks(18, Ref<ECB>(new ECB(2, 15)),
                                            Ref<ECB>(new ECB(2, 16)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(2, 11)),
                                            Ref<ECB>(new ECB(2, 12)))))));
    VERSIONS.push_back(Ref<Version>(new Version(6, intArray(2, 6, 34),
                                    Ref<ECBlocks>( new ECBlocks(18, Ref<ECB>(new ECB(2, 68)))),
                                    Ref<ECBlocks>( new ECBlocks(16, Ref<ECB>(new ECB(4, 27)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(4, 19)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(4, 15)))))));
    VERSIONS.push_back(Ref<Version>(new Version(7, intArray(3, 6, 22, 38),
                                    Ref<ECBlocks>( new ECBlocks(20, Ref<ECB>(new ECB(2, 78)))),
                                    Ref<ECBlocks>( new ECBlocks(18, Ref<ECB>(new ECB(4, 31)))),
                                    Ref<ECBlocks>( new ECBlocks(18, Ref<ECB>(new ECB(2, 14)),
                                            Ref<ECB>(new ECB(4, 15)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(4, 13)),
                                            Ref<ECB>(new ECB(1, 14)))))));
    VERSIONS.push_back(Ref<Version>(new Version(8, intArray(3, 6, 24, 42),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(2, 97)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(2, 38)),
                                            Ref<ECB>(new ECB(2, 39)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(4, 18)),
                                            Ref<ECB>(new ECB(2, 19)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(4, 14)),
                                            Ref<ECB>(new ECB(2, 15)))))));
    VERSIONS.push_back(Ref<Version>(new Version(9, intArray(3, 6, 26, 46),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(2, 116)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(3, 36)),
                                            Ref<ECB>(new ECB(2, 37)))),
                                    Ref<ECBlocks>( new ECBlocks(20, Ref<ECB>(new ECB(4, 16)),
                                            Ref<ECB>(new ECB(4, 17)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(4, 12)),
                                            Ref<ECB>(new ECB(4, 13)))))));
    VERSIONS.push_back(Ref<Version>(new Version(10, intArray(3, 6, 28, 50),
                                    Ref<ECBlocks>( new ECBlocks(18, Ref<ECB>(new ECB(2, 68)),
                                            Ref<ECB>(new ECB(2, 69)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(4, 43)),
                                            Ref<ECB>(new ECB(1, 44)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(6, 19)),
                                            Ref<ECB>(new ECB(2, 20)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(6, 15)),
                                            Ref<ECB>(new ECB(2, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(11, intArray(3, 6, 30, 54),
                                    Ref<ECBlocks>( new ECBlocks(20, Ref<ECB>(new ECB(4, 81)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(1, 50)),
                                            Ref<ECB>(new ECB(4, 51)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(4, 22)),
                                            Ref<ECB>(new ECB(4, 23)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(3, 12)),
                                            Ref<ECB>(new ECB(8, 13)))))));
    VERSIONS.push_back(Ref<Version>(new Version(12, intArray(3, 6, 32, 58),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(2, 92)),
                                            Ref<ECB>(new ECB(2, 93)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(6, 36)),
                                            Ref<ECB>(new ECB(2, 37)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(4, 20)),
                                            Ref<ECB>(new ECB(6, 21)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(7, 14)),
                                            Ref<ECB>(new ECB(4, 15)))))));
    VERSIONS.push_back(Ref<Version>(new Version(13, intArray(3, 6, 34, 62),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(4, 107)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(8, 37)),
                                            Ref<ECB>(new ECB(1, 38)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(8, 20)),
                                            Ref<ECB>(new ECB(4, 21)))),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(12, 11)),
                                            Ref<ECB>(new ECB(4, 12)))))));
    VERSIONS.push_back(Ref<Version>(new Version(14, intArray(4, 6, 26, 46, 66),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(3, 115)),
                                            Ref<ECB>(new ECB(1, 116)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(4, 40)),
                                            Ref<ECB>(new ECB(5, 41)))),
                                    Ref<ECBlocks>( new ECBlocks(20, Ref<ECB>(new ECB(11, 16)),
                                            Ref<ECB>(new ECB(5, 17)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(11, 12)),
                                            Ref<ECB>(new ECB(5, 13)))))));
    VERSIONS.push_back(Ref<Version>(new Version(15, intArray(4, 6, 26, 48, 70),
                                    Ref<ECBlocks>( new ECBlocks(22, Ref<ECB>(new ECB(5, 87)),
                                            Ref<ECB>(new ECB(1, 88)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(5, 41)),
                                            Ref<ECB>(new ECB(5, 42)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(5, 24)),
                                            Ref<ECB>(new ECB(7, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(11, 12)),
                                            Ref<ECB>(new ECB(7, 13)))))));
    VERSIONS.push_back(Ref<Version>(new Version(16, intArray(4, 6, 26, 50, 74),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(5, 98)),
                                            Ref<ECB>(new ECB(1, 99)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(7, 45)),
                                            Ref<ECB>(new ECB(3, 46)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(15, 19)),
                                            Ref<ECB>(new ECB(2, 20)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(3, 15)),
                                            Ref<ECB>(new ECB(13, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(17, intArray(4, 6, 30, 54, 78),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(1, 107)),
                                            Ref<ECB>(new ECB(5, 108)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(10, 46)),
                                            Ref<ECB>(new ECB(1, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(1, 22)),
                                            Ref<ECB>(new ECB(15, 23)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(2, 14)),
                                            Ref<ECB>(new ECB(17, 15)))))));
    VERSIONS.push_back(Ref<Version>(new Version(18, intArray(4, 6, 30, 56, 82),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(5, 120)),
                                            Ref<ECB>(new ECB(1, 121)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(9, 43)),
                                            Ref<ECB>(new ECB(4, 44)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(17, 22)),
                                            Ref<ECB>(new ECB(1, 23)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(2, 14)),
                                            Ref<ECB>(new ECB(19, 15)))))));
    VERSIONS.push_back(Ref<Version>(new Version(19, intArray(4, 6, 30, 58, 86),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(3, 113)),
                                            Ref<ECB>(new ECB(4, 114)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(3, 44)),
                                            Ref<ECB>(new ECB(11, 45)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(17, 21)),
                                            Ref<ECB>(new ECB(4, 22)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(9, 13)),
                                            Ref<ECB>(new ECB(16, 14)))))));
    VERSIONS.push_back(Ref<Version>(new Version(20, intArray(4, 6, 34, 62, 90),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(3, 107)),
                                            Ref<ECB>(new ECB(5, 108)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(3, 41)),
                                            Ref<ECB>(new ECB(13, 42)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(15, 24)),
                                            Ref<ECB>(new ECB(5, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(15, 15)),
                                            Ref<ECB>(new ECB(10, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(21, intArray(5, 6, 28, 50, 72, 94),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(4, 116)),
                                            Ref<ECB>(new ECB(4, 117)))),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(17, 42)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(17, 22)),
                                            Ref<ECB>(new ECB(6, 23)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(19, 16)),
                                            Ref<ECB>(new ECB(6, 17)))))));
    VERSIONS.push_back(Ref<Version>(new Version(22, intArray(5, 6, 26, 50, 74, 98),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(2, 111)),
                                            Ref<ECB>(new ECB(7, 112)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(17, 46)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(7, 24)),
                                            Ref<ECB>(new ECB(16, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(24, Ref<ECB>(new ECB(34, 13)))))));
    VERSIONS.push_back(Ref<Version>(new Version(23, intArray(5, 6, 30, 54, 78, 102),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(4, 121)),
                                            Ref<ECB>(new ECB(5, 122)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(4, 47)),
                                            Ref<ECB>(new ECB(14, 48)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(11, 24)),
                                            Ref<ECB>(new ECB(14, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(16, 15)),
                                            Ref<ECB>(new ECB(14, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(24, intArray(5, 6, 28, 54, 80, 106),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(6, 117)),
                                            Ref<ECB>(new ECB(4, 118)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(6, 45)),
                                            Ref<ECB>(new ECB(14, 46)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(11, 24)),
                                            Ref<ECB>(new ECB(16, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(30, 16)),
                                            Ref<ECB>(new ECB(2, 17)))))));
    VERSIONS.push_back(Ref<Version>(new Version(25, intArray(5, 6, 32, 58, 84, 110),
                                    Ref<ECBlocks>( new ECBlocks(26, Ref<ECB>(new ECB(8, 106)),
                                            Ref<ECB>(new ECB(4, 107)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(8, 47)),
                                            Ref<ECB>(new ECB(13, 48)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(7, 24)),
                                            Ref<ECB>(new ECB(22, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(22, 15)),
                                            Ref<ECB>(new ECB(13, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(26, intArray(5, 6, 30, 58, 86, 114),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(10, 114)),
                                            Ref<ECB>(new ECB(2, 115)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(19, 46)),
                                            Ref<ECB>(new ECB(4, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(28, 22)),
                                            Ref<ECB>(new ECB(6, 23)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(33, 16)),
                                            Ref<ECB>(new ECB(4, 17)))))));
    VERSIONS.push_back(Ref<Version>(new Version(27, intArray(5, 6, 34, 62, 90, 118),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(8, 122)),
                                            Ref<ECB>(new ECB(4, 123)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(22, 45)),
                                            Ref<ECB>(new ECB(3, 46)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(8, 23)),
                                            Ref<ECB>(new ECB(26, 24)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(12, 15)),
                                            Ref<ECB>(new ECB(28, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(28, intArray(6, 6, 26, 50, 74, 98, 122),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(3, 117)),
                                            Ref<ECB>(new ECB(10, 118)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(3, 45)),
                                            Ref<ECB>(new ECB(23, 46)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(4, 24)),
                                            Ref<ECB>(new ECB(31, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(11, 15)),
                                            Ref<ECB>(new ECB(31, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(29, intArray(6, 6, 30, 54, 78, 102, 126),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(7, 116)),
                                            Ref<ECB>(new ECB(7, 117)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(21, 45)),
                                            Ref<ECB>(new ECB(7, 46)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(1, 23)),
                                            Ref<ECB>(new ECB(37, 24)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(19, 15)),
                                            Ref<ECB>(new ECB(26, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(30, intArray(6, 6, 26, 52, 78, 104, 130),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(5, 115)),
                                            Ref<ECB>(new ECB(10, 116)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(19, 47)),
                                            Ref<ECB>(new ECB(10, 48)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(15, 24)),
                                            Ref<ECB>(new ECB(25, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(23, 15)),
                                            Ref<ECB>( new ECB(25, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(31, intArray(6, 6, 30, 56, 82, 108, 134),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(13, 115)),
                                            Ref<ECB>(new ECB(3, 116)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(2, 46)),
                                            Ref<ECB>(new ECB(29, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(42, 24)),
                                            Ref<ECB>(new ECB(1, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(23, 15)),
                                            Ref<ECB>(new ECB(28, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(32, intArray(6, 6, 34, 60, 86, 112, 138),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(17, 115)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(10, 46)),
                                            Ref<ECB>(new ECB(23, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(10, 24)),
                                            Ref<ECB>(new ECB(35, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(19, 15)),
                                            Ref<ECB>(new ECB(35, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(33, intArray(6, 6, 30, 58, 86, 114, 142),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(17, 115)),
                                            Ref<ECB>(new ECB(1, 116)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(14, 46)),
                                            Ref<ECB>(new ECB(21, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(29, 24)),
                                            Ref<ECB>(new ECB(19, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(11, 15)),
                                            Ref<ECB>(new ECB(46, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(34, intArray(6, 6, 34, 62, 90, 118, 146),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(13, 115)),
                                            Ref<ECB>(new ECB(6, 116)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(14, 46)),
                                            Ref<ECB>(new ECB(23, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(44, 24)),
                                            Ref<ECB>(new ECB(7, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(59, 16)),
                                            Ref<ECB>(new ECB(1, 17)))))));
    VERSIONS.push_back(Ref<Version>(new Version(35, intArray(7, 6, 30, 54, 78,
                                    102, 126, 150),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(12, 121)),
                                            Ref<ECB>(new ECB(7, 122)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(12, 47)),
                                            Ref<ECB>(new ECB(26, 48)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(39, 24)),
                                            Ref<ECB>(new ECB(14, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(22, 15)),
                                            Ref<ECB>(new ECB(41, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(36, intArray(7, 6, 24, 50, 76,
                                    102, 128, 154),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(6, 121)),
                                            Ref<ECB>(new ECB(14, 122)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(6, 47)),
                                            Ref<ECB>(new ECB(34, 48)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(46, 24)),
                                            Ref<ECB>(new ECB(10, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(2, 15)),
                                            Ref<ECB>(new ECB(64, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(37, intArray(7, 6, 28, 54, 80,
                                    106, 132, 158),
                                    Ref<ECBlocks>( new ECBlocks(30,Ref<ECB>(new ECB(17, 122)),
                                            Ref<ECB>(new ECB(4, 123)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(29, 46)),
                                            Ref<ECB>(new ECB(14, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(49, 24)),
                                            Ref<ECB>(new ECB(10, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(24, 15)),
                                            Ref<ECB>(new ECB(46, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(38, intArray(7, 6, 32, 58, 84,
                                    110, 136, 162),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(4, 122)),
                                            Ref<ECB>(new ECB(18, 123)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(13, 46)),
                                            Ref<ECB>(new ECB(32, 47)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(48, 24)),
                                            Ref<ECB>(new ECB(14, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(42, 15)),
                                            Ref<ECB>(new ECB(32, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(39, intArray(7, 6, 26, 54, 82,
                                    110, 138, 166),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(20, 117)),
                                            Ref<ECB>(new ECB(4, 118)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(40, 47)),
                                            Ref<ECB>(new ECB(7, 48)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(43, 24)),
                                            Ref<ECB>(new ECB(22, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(10, 15)),
                                            Ref<ECB>(new ECB(67, 16)))))));
    VERSIONS.push_back(Ref<Version>(new Version(40, intArray(7, 6, 30, 58, 86,
                                    114, 142, 170),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(19, 118)),
                                            Ref<ECB>(new ECB(6, 119)))),
                                    Ref<ECBlocks>( new ECBlocks(28, Ref<ECB>(new ECB(18, 47)),
                                            Ref<ECB>(new ECB(31, 48)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(34, 24)),
                                            Ref<ECB>(new ECB(34, 25)))),
                                    Ref<ECBlocks>( new ECBlocks(30, Ref<ECB>(new ECB(20, 15)),
                                            Ref<ECB>(new ECB(61, 16)))))));
    return VERSIONS.size();
}
}
}
