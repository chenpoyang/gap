#ifndef MASKUTIL_H
#define MASKUTIL_H

#include "Counted.h"
#include "ByteMatrix.h"
#include "QRCode.h"
namespace zhixin {
namespace qrcode {
class MaskUtil :public Counted {
public:
    MaskUtil();
    virtual ~MaskUtil();

    static int applyMaskPenaltyRule1(Ref<ByteMatrix> matrix);
    static int applyMaskPenaltyRule2(Ref<ByteMatrix> matrix);
    static int applyMaskPenaltyRule3(Ref<ByteMatrix> matrix);
    static int applyMaskPenaltyRule4(Ref<ByteMatrix> matrix);
    static bool getDataMaskBit(int maskPattern, int x, int y);
    static int applyMaskPenaltyRule1Internal(Ref<ByteMatrix> matrix, bool isHorizontal);
protected:
private:
};
}
}
#endif // MASKUTIL_H
