#include "Encoder.h"

using namespace std;
#include <string>



namespace zhixin {
namespace qrcode {
Encoder::Encoder() {
    //ctor
}

Encoder::~Encoder() {
    //dtor
}

int Encoder::ALPHANUMERIC_TABLE[96]= {
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  // 0x00-0x0f
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  // 0x10-0x1f
      36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43,  // 0x20-0x2f
      0,   1,  2,  3,  4,  5,  6,  7,  8,  9, 44, -1, -1, -1, -1, -1,  // 0x30-0x3f
      -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,  // 0x40-0x4f
      25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1  // 0x50-0x5f
  };

string Encoder::DEFAULT_BYTE_MODE_ENCODING = "UTF-8";//"ISO-8859-1";

// The mask penalty calculation is complicated.  See Table 21 of JISX0510:2004 (p.45) for details.
// Basically it applies four rules and summate all penalties.
int Encoder::calculateMaskPenalty(Ref<ByteMatrix> matrix) {
    int penalty = 0;
    penalty += MaskUtil::applyMaskPenaltyRule1(matrix);
    penalty += MaskUtil::applyMaskPenaltyRule2(matrix);
    penalty += MaskUtil::applyMaskPenaltyRule3(matrix);
    penalty += MaskUtil::applyMaskPenaltyRule4(matrix);
    return penalty;
}

/// <summary>  Encode "bytes" with the error correction level "ecLevel". The encoding mode will be chosen
/// internally by chooseMode(). On success, store the result in "qrCode".
///
/// We recommend you to use QRCode.EC_LEVEL_L (the lowest level) for
/// "getECLevel" since our primary use is to show QR code on desktop screens. We don't need very
/// strong error correction for this purpose.
///
/// Note that there is no way to encode bytes in MODE_KANJI. We might want to add EncodeWithMode()
/// with which clients can specify the encoding mode. For now, we don't need the functionality.
/// </summary>
/*void  Encoder::encode(string content, Ref<ErrorCorrectionLevel> ecLevel, Ref<QRCode> qrCode)
		{
			encode(content, ecLevel, null, qrCode);
		}
*/
void  Encoder::encode(string content, Ref<ErrorCorrectionLevel> ecLevel, string hints, Ref<QRCode> qrCode) {

    /*
    string encoding = hints == null?null:(string) hints[EncodeHintType.CHARACTER_SET];
    if (encoding == null)
    {
    	encoding = DEFAULT_BYTE_MODE_ENCODING;
    }
    */
    string encoding = DEFAULT_BYTE_MODE_ENCODING;
    // Step 1: Choose the mode (encoding).

    //Ref<Mode> mode = chooseMode(content, encoding);
    Ref<Mode> mode = Ref<Mode>(Mode::BYTE);


    // Step 2: Append "bytes" into "dataBits" in appropriate encoding.
    Ref<BitVector> dataBits = Ref<BitVector>(new BitVector());
    appendBytes(content, mode, dataBits, encoding);
    // Step 3: Initialize QR code that can contain "dataBits".
    int numInputBytes = dataBits->sizeInBytes();
    initQRCode(numInputBytes, ecLevel, mode, qrCode);

    // Step 4: Build another bit vector that contains header and data.
    Ref<BitVector> headerAndDataBits = Ref<BitVector>(new BitVector());

    // Step 4.5: Append ECI message if applicable
    if (mode->Name() == Mode::BYTE->Name()  && !(DEFAULT_BYTE_MODE_ENCODING == encoding)) {
        common::CharacterSetECI* eci = common::CharacterSetECI::getCharacterSetECIByName(encoding);
        if (eci != NULL) {
            appendECI(eci, headerAndDataBits);
        }
    }

    appendModeInfo(mode, headerAndDataBits);

    //int numLetters = mode == Mode::BYTE ? dataBits->sizeInBytes():content.length();
    int numLetters = mode->isEquals(Ref<Mode>(Mode::BYTE))  ? dataBits->sizeInBytes():content.length();
    appendLengthInfo(numLetters, qrCode->version, mode, headerAndDataBits);
    headerAndDataBits->appendBitVector(dataBits);

    // Step 5: Terminate the bits properly.
    terminateBits(qrCode->numDataBytes, headerAndDataBits);

    // Step 6: Interleave data bits with error correction code.
    Ref<BitVector> finalBits = Ref<BitVector>(new BitVector());
    interleaveWithECBytes(headerAndDataBits, qrCode->numTotalBytes, qrCode->numDataBytes, qrCode->numRSBlocks, finalBits);

    // Step 7: Choose the mask pattern and set to "qrCode".
    Ref<ByteMatrix> matrix = Ref<ByteMatrix>(new ByteMatrix(qrCode->matrixWidth, qrCode->matrixWidth));
    qrCode->maskPattern = chooseMaskPattern(finalBits, qrCode->ecLevel, qrCode->version, matrix);

    // Step 8.  Build the matrix and set it to "qrCode".
    MatrixUtil::buildMatrix(finalBits, qrCode->ecLevel, qrCode->version, qrCode->maskPattern, matrix);
    qrCode->setMatrix(matrix);
    // Step 9.  Make sure we have a valid QR Code.
    if (!qrCode->getValid()) {
        throw Exception("Invalid QR code: " /*+ qrCode->ToString()*/);
    }
}
//licheng add
void  Encoder::encode( char* pcontent, int length,Ref<ErrorCorrectionLevel> ecLevel, string hints, Ref<QRCode> qrCode) {

    /*
    string encoding = hints == null?null:(string) hints[EncodeHintType.CHARACTER_SET];
    if (encoding == null)
    {
    	encoding = DEFAULT_BYTE_MODE_ENCODING;
    }
    */
    string encoding = DEFAULT_BYTE_MODE_ENCODING;
    // Step 1: Choose the mode (encoding).

    //Ref<Mode> mode = chooseMode(content, encoding);
    Ref<Mode> mode = Ref<Mode>(Mode::BYTE);


    // Step 2: Append "bytes" into "dataBits" in appropriate encoding.
    Ref<BitVector> dataBits = Ref<BitVector>(new BitVector());
//    appendBytes(content, mode, dataBits, encoding);fixme
    int len = length;
    const char* tmp = pcontent;
    for (int i = 0; i < len; ++i) {
        dataBits->appendBits(tmp[i], 8);
    }
    // Step 3: Initialize QR code that can contain "dataBits".
    int numInputBytes = dataBits->sizeInBytes();
    initQRCode(numInputBytes, ecLevel, mode, qrCode);

    // Step 4: Build another bit vector that contains header and data.
    Ref<BitVector> headerAndDataBits = Ref<BitVector>(new BitVector());

    // Step 4.5: Append ECI message if applicable
    if (mode->Name() == Mode::BYTE->Name()  && !(DEFAULT_BYTE_MODE_ENCODING == encoding)) {
        common::CharacterSetECI* eci = common::CharacterSetECI::getCharacterSetECIByName(encoding);
        if (eci != NULL) {
            appendECI(eci, headerAndDataBits);
        }
    }

    appendModeInfo(mode, headerAndDataBits);

    //int numLetters = mode == Mode::BYTE ? dataBits->sizeInBytes():content.length();
    int numLetters = mode->isEquals(Ref<Mode>(Mode::BYTE))  ? dataBits->sizeInBytes():length;
    appendLengthInfo(numLetters, qrCode->version, mode, headerAndDataBits);
    headerAndDataBits->appendBitVector(dataBits);

    // Step 5: Terminate the bits properly.
    terminateBits(qrCode->numDataBytes, headerAndDataBits);

    // Step 6: Interleave data bits with error correction code.
    Ref<BitVector> finalBits = Ref<BitVector>(new BitVector());
    interleaveWithECBytes(headerAndDataBits, qrCode->numTotalBytes, qrCode->numDataBytes, qrCode->numRSBlocks, finalBits);

    // Step 7: Choose the mask pattern and set to "qrCode".
    Ref<ByteMatrix> matrix = Ref<ByteMatrix>(new ByteMatrix(qrCode->matrixWidth, qrCode->matrixWidth));
    qrCode->maskPattern = chooseMaskPattern(finalBits, qrCode->ecLevel, qrCode->version, matrix);

    // Step 8.  Build the matrix and set it to "qrCode".
    MatrixUtil::buildMatrix(finalBits, qrCode->ecLevel, qrCode->version, qrCode->maskPattern, matrix);
    qrCode->setMatrix(matrix);
    // Step 9.  Make sure we have a valid QR Code.
    if (!qrCode->getValid()) {
        throw Exception("Invalid QR code: " /*+ qrCode->ToString()*/);
    }
}
/// <returns> the code point of the table used in alphanumeric mode or
/// -1 if there is no corresponding code in the table.
/// </returns>
//using
int Encoder::getAlphanumericCode(int code) {
    //if (code < (sizeof(ALPHANUMERIC_TABLE)/sizeof(ALPHANUMERIC_TABLE[0])))
    if(code < 96) {
        return ALPHANUMERIC_TABLE[code];
    }
    return - 1;
}

Ref<Mode> Encoder::chooseMode(string content) {
    return chooseMode(content, NULL);
}

/// <summary> Choose the best mode by examining the content. Note that 'encoding' is used as a hint;
/// if it is Shift_JIS, and the input is only double-byte Kanji, then we return {@link Mode#KANJI}.
/// </summary>
Ref<Mode> Encoder::chooseMode(string content, string encoding) {
    /*
    			if ("Shift_JIS".Equals(encoding))
    			{
    				// Choose Kanji mode if all input are double-byte characters
    				return isOnlyDoubleByteKanji(content)?Mode.KANJI:Mode.BYTE;
    			}
    */
    bool hasNumeric = false;
    bool hasAlphanumeric = false;
    for (int i = 0; i < (int)(content.length()); ++i) {
        char c = content[i];
        if (c >= '0' && c <= '9') {
            hasNumeric = true;
        } else if (getAlphanumericCode(c) != - 1) {
            hasAlphanumeric = true;
        } else {
            return Ref<Mode>(Mode::BYTE);
        }
    }
    if (hasAlphanumeric) {
        return Ref<Mode>(Mode::ALPHANUMERIC);
    } else if (hasNumeric) {
        return Ref<Mode>(Mode::NUMERIC);
    }
    return Ref<Mode>(Mode::BYTE);
}
/*
bool Encoder::isOnlyDoubleByteKanji(string content)
		{
			char[] bytes;
			try
			{
				//UPGRADE_TODO: Method 'java.lang.String.getBytes' was converted to 'System.Text.Encoding.GetEncoding(string).GetBytes(string)' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javalangStringgetBytes_javalangString'"
				bytes = SupportClass.ToSByteArray(System.Text.Encoding.GetEncoding("Shift_JIS").GetBytes(content));
			}
			catch (System.IO.IOException)
			{
				return false;
			}
			int length = bytes.Length;
			if (length % 2 != 0)
			{
				return false;
			}
			for (int i = 0; i < length; i += 2)
			{
				int byte1 = bytes[i] & 0xFF;
				if ((byte1 < 0x81 || byte1 > 0x9F) && (byte1 < 0xE0 || byte1 > 0xEB))
				{
					return false;
				}
			}
			return true;
			return false;
		}
*/
int Encoder::chooseMaskPattern(Ref<BitVector> bits, Ref<ErrorCorrectionLevel> ecLevel, int version, Ref<ByteMatrix> matrix) {

    //int minPenalty = System.Int32.MaxValue; // Lower penalty is better.
    unsigned int tmp =0;
    int minPenalty  = (~tmp)/2;
    int bestMaskPattern = - 1;
    // We try all mask patterns to choose the best one.
    for (int maskPattern = 0; maskPattern < QRCode::NUM_MASK_PATTERNS; maskPattern++) {
        MatrixUtil::buildMatrix(bits, ecLevel, version, maskPattern, matrix);
        int penalty = calculateMaskPenalty(matrix);
        if (penalty < minPenalty) {
            minPenalty = penalty;
            bestMaskPattern = maskPattern;
        }
    }
    return bestMaskPattern;
}

/// <summary> Initialize "qrCode" according to "numInputBytes", "ecLevel", and "mode". On success,
/// modify "qrCode".
/// </summary>
void  Encoder::initQRCode(int numInputBytes, Ref<ErrorCorrectionLevel> ecLevel, Ref<Mode> mode, Ref<QRCode> qrCode) {
    qrCode->ecLevel = ecLevel;
    qrCode->mode = mode;

    // In the following comments, we use numbers of Version 7-H.
    for (int versionNum = 1; versionNum <= 40; versionNum++) {
        Version* version = Version::getVersionForNumber(versionNum);
        // numBytes = 196
        int numBytes = version->TotalCodewords();
        // getNumECBytes = 130
        Ref<ECBlocks> ecBlocks = version->getECBlocksForLevel(ecLevel);
        int numEcBytes = ecBlocks->TotalECCodewords();
        // getNumRSBlocks = 5
        int numRSBlocks = ecBlocks->NumBlocks();
        // getNumDataBytes = 196 - 130 = 66
        int numDataBytes = numBytes - numEcBytes;
        // We want to choose the smallest version which can contain data of "numInputBytes" + some
        // extra bits for the header (mode info and length info). The header can be three bytes
        // (precisely 4 + 16 bits) at most. Hence we do +3 here.
        if (numDataBytes >= numInputBytes + 3) {
            // Yay, we found the proper rs block info!
            qrCode->version = versionNum;
            qrCode->numTotalBytes = numBytes;
            qrCode->numDataBytes = numDataBytes;
            qrCode->numRSBlocks = numRSBlocks;
            // getNumECBytes = 196 - 66 = 130
            qrCode->numECBytes = numEcBytes;
            // matrix width = 21 + 6 * 4 = 45
            qrCode->matrixWidth = version->DimensionForVersion();
            return ;
        }
    }
    throw Exception("Cannot find proper rs block info (input data too big?)");
}

/// <summary> Terminate bits as described in 8.4.8 and 8.4.9 of JISX0510:2004 (p.24).</summary>
void  Encoder::terminateBits(int numDataBytes, Ref<BitVector> bits) {
    int capacity = numDataBytes << 3;
    if (bits->size() > capacity) {
        throw Exception("data bits cannot fit in the QR Code" /*+ bits->size() + " > " + capacity*/);
    }
    // Append termination bits. See 8.4.8 of JISX0510:2004 (p.24) for details.
    // TODO: srowen says we can remove this for loop, since the 4 terminator bits are optional if
    // the last byte has less than 4 bits left. So it amounts to padding the last byte with zeroes
    // either way.
    for (int i = 0; i < 4 && bits->size() < capacity; ++i) {
        bits->appendBit(0);
    }
    int numBitsInLastByte = bits->size() % 8;
    // If the last byte isn't 8-bit aligned, we'll add padding bits.
    if (numBitsInLastByte > 0) {
        int numPaddingBits = 8 - numBitsInLastByte;
        for (int i = 0; i < numPaddingBits; ++i) {
            bits->appendBit(0);
        }
    }
    // Should be 8-bit aligned here.
    if (bits->size() % 8 != 0) {
        throw Exception("Number of bits is not a multiple of 8");
    }
    // If we have more space, we'll fill the space with padding patterns defined in 8.4.9 (p.24).
    int numPaddingBytes = numDataBytes - bits->sizeInBytes();
    for (int i = 0; i < numPaddingBytes; ++i) {
        if (i % 2 == 0) {
            bits->appendBits(0xec, 8);
        } else {
            bits->appendBits(0x11, 8);
        }
    }
    if (bits->size() != capacity) {
        throw  Exception("Bits size does not equal capacity");
    }
}

/// <summary> Get number of data bytes and number of error correction bytes for block id "blockID". Store
/// the result in "numDataBytesInBlock", and "numECBytesInBlock". See table 12 in 8.5.1 of
/// JISX0510:2004 (p.30)
/// </summary>
void  Encoder::getNumDataBytesAndNumECBytesForBlockID(int numTotalBytes, int numDataBytes, int numRSBlocks, int blockID, int numDataBytesInBlock[], int numECBytesInBlock[]) {
    if (blockID >= numRSBlocks) {
        throw Exception("Block ID too large");
    }
    // numRsBlocksInGroup2 = 196 % 5 = 1
    int numRsBlocksInGroup2 = numTotalBytes % numRSBlocks;
    // numRsBlocksInGroup1 = 5 - 1 = 4
    int numRsBlocksInGroup1 = numRSBlocks - numRsBlocksInGroup2;
    // numTotalBytesInGroup1 = 196 / 5 = 39
    int numTotalBytesInGroup1 = numTotalBytes / numRSBlocks;
    // numTotalBytesInGroup2 = 39 + 1 = 40
    int numTotalBytesInGroup2 = numTotalBytesInGroup1 + 1;
    // numDataBytesInGroup1 = 66 / 5 = 13
    int numDataBytesInGroup1 = numDataBytes / numRSBlocks;
    // numDataBytesInGroup2 = 13 + 1 = 14
    int numDataBytesInGroup2 = numDataBytesInGroup1 + 1;
    // numEcBytesInGroup1 = 39 - 13 = 26
    int numEcBytesInGroup1 = numTotalBytesInGroup1 - numDataBytesInGroup1;
    // numEcBytesInGroup2 = 40 - 14 = 26
    int numEcBytesInGroup2 = numTotalBytesInGroup2 - numDataBytesInGroup2;
    // Sanity checks.
    // 26 = 26
    if (numEcBytesInGroup1 != numEcBytesInGroup2) {
        throw Exception("EC bytes mismatch");
    }
    // 5 = 4 + 1.
    if (numRSBlocks != numRsBlocksInGroup1 + numRsBlocksInGroup2) {
        throw Exception("RS blocks mismatch");
    }
    // 196 = (13 + 26) * 4 + (14 + 26) * 1
    if (numTotalBytes != ((numDataBytesInGroup1 + numEcBytesInGroup1) * numRsBlocksInGroup1) + ((numDataBytesInGroup2 + numEcBytesInGroup2) * numRsBlocksInGroup2)) {
        throw Exception("Total bytes mismatch");
    }

    if (blockID < numRsBlocksInGroup1) {
        numDataBytesInBlock[0] = numDataBytesInGroup1;
        numECBytesInBlock[0] = numEcBytesInGroup1;
    } else {
        numDataBytesInBlock[0] = numDataBytesInGroup2;
        numECBytesInBlock[0] = numEcBytesInGroup2;
    }
}

/// <summary> Interleave "bits" with corresponding error correction bytes. On success, store the result in
/// "result". The interleave rule is complicated. See 8.6 of JISX0510:2004 (p.37) for details.
/// </summary>
void  Encoder::interleaveWithECBytes(Ref<BitVector> bits, int numTotalBytes, int numDataBytes, int numRSBlocks, Ref<BitVector> result) {

    // "bits" must have "getNumDataBytes" bytes of data.
    if (bits->sizeInBytes() != numDataBytes) {
        throw Exception("Number of bits and data bytes does not match");
    }

    // Step 1.  Divide data bytes into blocks and generate error correction bytes for them. We'll
    // store the divided data bytes blocks and error correction bytes blocks into "blocks".
    int dataBytesOffset = 0;
    int maxNumDataBytes = 0;
    int maxNumEcBytes = 0;

    // Since, we know the number of reedsolmon blocks, we can initialize the vector with the number.
    //System.Collections.ArrayList blocks = System.Collections.ArrayList.Synchronized(new System.Collections.ArrayList(numRSBlocks));
    vector<Ref<BlockPair> > blocks;
    for (int i = 0; i < numRSBlocks; ++i) {
        int numDataBytesInBlock[1] ;
        int numEcBytesInBlock[1];
        getNumDataBytesAndNumECBytesForBlockID(numTotalBytes, numDataBytes, numRSBlocks, i, numDataBytesInBlock, numEcBytesInBlock);

        Ref<ByteArray> dataBytes = Ref<ByteArray>(new ByteArray());
        dataBytes->set_Renamed(bits->Array(), dataBytesOffset, numDataBytesInBlock[0]);
        Ref<ByteArray> ecBytes = generateECBytes(dataBytes, numEcBytesInBlock[0]);
        //blocks.Add(new BlockPair(dataBytes, ecBytes));
        blocks.push_back(Ref<BlockPair>(new BlockPair(dataBytes, ecBytes)));

        maxNumDataBytes =max(maxNumDataBytes, dataBytes->size());
        maxNumEcBytes = max(maxNumEcBytes, ecBytes->size());
        dataBytesOffset += numDataBytesInBlock[0];
    }
    if (numDataBytes != dataBytesOffset) {
        throw Exception("Data bytes does not match offset");
    }

    // First, place data blocks.
    for (int i = 0; i < maxNumDataBytes; ++i) {
        for (int j = 0; j < (int)(blocks.size()); ++j) {
            Ref<ByteArray> dataBytes = (blocks[j])->dataBytes;
            if (i < dataBytes->size()) {
                result->appendBits(dataBytes->at(i), 8);
            }
        }
    }
    // Then, place error correction blocks.
    for (int i = 0; i < maxNumEcBytes; ++i) {
        for (int j = 0; j < (int)(blocks.size()); ++j) {
            Ref<ByteArray> ecBytes = (blocks[j])->errorCorrectionBytes;
            if (i < ecBytes->size()) {
                result->appendBits(ecBytes->at(i), 8);
            }
        }
    }
    if (numTotalBytes != result->sizeInBytes()) {
        // Should be same.
        throw Exception("Interleaving error: " /*+ numTotalBytes + " and " + result->sizeInBytes() + " differ."*/);
    }
}

Ref<ByteArray> Encoder::generateECBytes(Ref<ByteArray> dataBytes, int numEcBytesInBlock) {
    int numDataBytes = dataBytes->size();
    ArrayRef<int> toEncode(numDataBytes + numEcBytesInBlock);
    for (int i = 0; i < numDataBytes; i++) {
        int tmp = dataBytes->at(i);
        toEncode[i] = tmp;
    }
    //new ReedSolomonEncoder(GF256.QR_CODE_FIELD).encode(toEncode, numEcBytesInBlock);
    Ref<ReedSolomonEncoder> rse = Ref<ReedSolomonEncoder>(new ReedSolomonEncoder(GF256::QR_CODE_FIELD));
    rse->encode(toEncode, numEcBytesInBlock);

    Ref<ByteArray> ecBytes = Ref<ByteArray>(new ByteArray(numEcBytesInBlock));
    for (int i = 0; i < numEcBytesInBlock; i++) {
        ecBytes->set_Renamed(i, toEncode[numDataBytes + i]);
    }
    return ecBytes;
}

/// <summary> Append mode info. On success, store the result in "bits".</summary>
void  Encoder::appendModeInfo(Ref<Mode> mode, Ref<BitVector> bits) {
    bits->appendBits(mode->Bits(), 4);
}


/// <summary> Append length info. On success, store the result in "bits".</summary>
void Encoder::appendLengthInfo(int numLetters, int version, Ref<Mode> mode, Ref<BitVector> bits) {
    int numBits = mode->getCharacterCountBits(Version::getVersionForNumber(version));
    if (numLetters > ((1 << numBits) - 1)) {
        throw Exception(numLetters + "is bigger than" + ((1 << numBits) - 1));
    }
    bits->appendBits(numLetters, numBits);
}

/// <summary> Append "bytes" in "mode" mode (encoding) into "bits". On success, store the result in "bits".</summary>
void  Encoder::appendBytes(string content, Ref<Mode> mode, Ref<BitVector> bits, string encoding) {
    //if (mode->isEquals(Ref<Mode>(Mode::NUMERIC)))
    if (mode->Name() == Mode::NUMERIC->Name()) {
        appendNumericBytes(content, bits);
    }
    //else if (mode->isEquals(Ref<Mode>(Mode::ALPHANUMERIC)))
    else if (mode->Name() == Mode::ALPHANUMERIC->Name()) {
        appendAlphanumericBytes(content, bits);
    }
    //else if (mode->isEquals(Ref<Mode>(Mode::BYTE)))
    else if (mode->Name() == Mode::BYTE->Name()) {
        append8BitBytes(content, bits, encoding);
    }
    /*	else if (mode->isEquals(Ref<Mode>(Mode::KANJI)))
    	{
    		appendKanjiBytes(content, bits);
    	}*/
    else {
        string ex = "Invalid mode: " + mode->Name();
        throw Exception(ex.c_str());
    }
}

void  Encoder::appendNumericBytes(string content, Ref<BitVector> bits) {
    int length = content.length();
    int i = 0;
    while (i < length) {
        int num1 = content[i] - '0';
        if (i + 2 < length) {
            // Encode three numeric letters in ten bits.
            int num2 = content[i + 1] - '0';
            int num3 = content[i + 2] - '0';
            bits->appendBits(num1 * 100 + num2 * 10 + num3, 10);
            i += 3;
        } else if (i + 1 < length) {
            // Encode two numeric letters in seven bits.
            int num2 = content[i + 1] - '0';
            bits->appendBits(num1 * 10 + num2, 7);
            i += 2;
        } else {
            // Encode one numeric letter in four bits.
            bits->appendBits(num1, 4);
            i++;
        }
    }
}

void  Encoder::appendAlphanumericBytes(string content, Ref<BitVector> bits) {
    int length = content.length();
    int i = 0;
    while (i < length) {
        int code1 = getAlphanumericCode(content[i]);
        if (code1 == - 1) {
            throw Exception("code1 == -1");
        }
        if (i + 1 < length) {
            int code2 = getAlphanumericCode(content[i + 1]);
            if (code2 == - 1) {
                throw Exception("code2 == -1");
            }
            // Encode two alphanumeric letters in 11 bits.
            bits->appendBits(code1 * 45 + code2, 11);
            i += 2;
        } else {
            // Encode one alphanumeric letter in six bits.
            bits->appendBits(code1, 6);
            i++;
        }
    }
}

void  Encoder::append8BitBytes(string content, Ref<BitVector> bits, string encoding) {

    //UPGRADE_TODO: Method 'java.lang.String.getBytes' was converted to 'System.Text.Encoding.GetEncoding(string).GetBytes(string)' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javalangStringgetBytes_javalangString'"
    int len = content.length();
    const char* tmp = content.c_str();
    /*       char in[len];
           for(int i=0;i<(len);i++){
               in[i] = tmp[i];
           }
           char out[len*8];

           convert(out, (size_t)(len*8), in, (size_t)len, "UTF-8", "ASCII");
    */
// ArrayRef<char> bytes = SupportClass::ToSByteArray(Encoder::HexStrToByte(content));


    for (int i = 0; i < len; ++i) {
        bits->appendBits(tmp[i], 8);
    }
}
/*!
 对字符串进行语言编码转换
 param from  原始编码，比如"GB2312",的按照iconv支持的写
 param to      转换的目的编码
 param save  转换后的数据保存到这个指针里，需要在外部分配内存
 param savelen 存储转换后数据的内存大小
 param src      原始需要转换的字符串
 param srclen    原始字符串长度
 */
int Encoder::convert(char *out, size_t outlen, char* in, size_t inlen, const char *tocode, const char* fromcode) {
    iconv_t cd;
    size_t olen = outlen;
    cd = iconv_open(tocode,fromcode);
    if(cd == (iconv_t)-1) return -1;

    if(iconv(cd,&in, &inlen, &out, &outlen) == (size_t)-1)return -1;

    iconv_close(cd);
    return olen - outlen;

}
/*
void  Encoder::appendKanjiBytes(string content, Ref<BitVector> bits)
		{
			sbyte[] bytes;
			try
			{
				//UPGRADE_TODO: Method 'java.lang.String.getBytes' was converted to 'System.Text.Encoding.GetEncoding(string).GetBytes(string)' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javalangStringgetBytes_javalangString'"
				bytes = SupportClass.ToSByteArray(System.Text.Encoding.GetEncoding("Shift_JIS").GetBytes(content));
			}
			catch (System.IO.IOException uee)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1043'"
				throw new WriterException(uee.ToString());
			}
			int length = bytes.Length;
			for (int i = 0; i < length; i += 2)
			{
				int byte1 = bytes[i] & 0xFF;
				int byte2 = bytes[i + 1] & 0xFF;
				int code = (byte1 << 8) | byte2;
				int subtracted = - 1;
				if (code >= 0x8140 && code <= 0x9ffc)
				{
					subtracted = code - 0x8140;
				}
				else if (code >= 0xe040 && code <= 0xebbf)
				{
					subtracted = code - 0xc140;
				}
				if (subtracted == - 1)
				{
					throw new WriterException("Invalid byte sequence");
				}
				int encoded = ((subtracted >> 8) * 0xc0) + (subtracted & 0xff);
				bits.appendBits(encoded, 13);
			}
		}
*/
void  Encoder::appendECI(common::CharacterSetECI* eci, Ref<BitVector> bits) {
    bits->appendBits(Mode::ECI->Bits(), 4);
    // This is correct for values up to 127, which is all we need now.
    bits->appendBits(eci->getValue(), 8);
}
}
}
