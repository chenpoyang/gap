#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <sys/shm.h>
#include <sys/sem.h>
#include "shmfifo.h"

int sem_create(key_t key)
{
    int semid;
    semid = semget(key, 1, IPC_CREAT | IPC_EXCL | 0666);
    if (semid == -1)
        ERR_EXIT("semget");

    return semid;
}

int sem_open(key_t key)
{
    int semid;
    semid = semget(key, 0, 0);
    if (semid == -1)
        ERR_EXIT("semget");

    return semid;
}

int sem_setval(int semid, int val)
{
    union semun su;
    su.val = val;
    int ret;
    ret = semctl(semid, 0, SETVAL, su);
    if (ret == -1)
        ERR_EXIT("sem_setval");

    return 0;
}

int sem_getval(int semid)
{
    int ret;
    ret = semctl(semid, 0, GETVAL, 0);
    if (ret == -1)
        ERR_EXIT("sem_getval");

    return ret;
}

int sem_d(int semid)
{
    int ret;
    ret = semctl(semid, 0, IPC_RMID, 0);
    if (ret == -1)
        ERR_EXIT("semctl");

    return 0;
}

int sem_p(int semid)
{
    struct sembuf sb = {0, -1, 0};
    int ret;
    ret = semop(semid, &sb, 1);
    if ( ret == -1)
        ERR_EXIT("semop");

    return ret;
}

int sem_v(int semid)
{
    struct sembuf sb = {0, 1, 0};
    int ret;
    ret = semop(semid, &sb, 1);
    if ( ret == -1)
        ERR_EXIT("semop");

    return ret;
}

/* 向共享内存中写入一块数据，即，使p_payload指针便宜相应的长度 */
void shmfifo_put(shmfifo_t *fifo, const void *buf)
{
    sem_p(fifo->sem_full);
    sem_p(fifo->sem_mutex);

    memcpy(fifo->p_payload + fifo->p_shm->blksize*fifo->p_shm->wd_idx,buf, fifo->p_shm->blksize);

    /*生产完数据之后，头部的wr_index需要更新，+1,
      但是由于我们实现的是环形缓冲区，所以再模上总块数blocks */
    fifo->p_shm->wd_idx = (fifo->p_shm->wd_idx+1) % fifo->p_shm->blocks;

    sem_v(fifo->sem_mutex);
    sem_v(fifo->sem_empty);
}

/* 从共享内存中读一块数据，与写操作类似 */
void shmfifo_get(shmfifo_t *fifo, void *buf)
{
    sem_p(fifo->sem_empty);
    sem_p(fifo->sem_mutex);

    memcpy(buf, fifo->p_payload + fifo->p_shm->blksize*fifo->p_shm->rd_idx, fifo->p_shm->blksize);
     /*生产完数据之后，头部的wr_index需要更新，+1,
       但是由于我们实现的是环形缓冲区，所以再模上总块数blocks */
    fifo->p_shm->rd_idx = (fifo->p_shm->rd_idx+1) % fifo->p_shm->blocks;

    sem_v(fifo->sem_mutex);
    sem_v(fifo->sem_full);
}

/* 程序结束时，需要执行shmfifo_destroy操作 */
void shmfifo_destroy(shmfifo_t *fifo)
{
    if (fifo != NULL && shmget(fifo->p_shm->key, 0, 0) != -1)
    {
        sem_d(fifo->sem_mutex);
        sem_d(fifo->sem_full);
        sem_d(fifo->sem_empty);

        shmdt(fifo->p_shm);
        shmctl(fifo->shmid, IPC_RMID, 0);

        free(fifo);
        fifo = NULL;
    }

    //the share memory may be released, but fifo not free yet
    if (fifo != NULL)
    {
        free(fifo);
        fifo = NULL;
    }
}

shmfifo_t *shmfifo_init(const int key, const int blksize, int blocks)
{
    shmfifo_t *fifo = (shmfifo_t *)malloc(sizeof(shmfifo_t));
    assert(fifo != NULL);
    memset(fifo, 0, sizeof(shmfifo_t));

    /* 先已打开方式调用shmget，若打开失败即说明还未创建，然后创建共享内存 */
    int shmid;
    shmid = shmget(key, 0, 0);
    int size = sizeof(shmhead_t) + blksize * blocks;
    if (shmid == -1)
    {
        fifo->shmid = shmget(key, size, IPC_CREAT | 0666);
        if (fifo->shmid == -1)
        {
            ERR_EXIT("shmget");
        }

        fifo->p_shm = (shmhead_t *)shmat(fifo->shmid, NULL, 0);
        if (fifo->p_shm == (shmhead_t *)-1)
            ERR_EXIT("shmat");

        /*创建三个信号量,两个信号量的key是不能相同的*/
        fifo->sem_mutex = sem_create(key);
        fifo->sem_full = sem_create(key + 100);
        fifo->sem_empty = sem_create(key + 200);

        /*将信号量初始化*/
        sem_setval(fifo->sem_mutex, 1);
        sem_setval(fifo->sem_full, blocks);
        sem_setval(fifo->sem_empty, 0);

        fifo->p_shm->key = key;
        fifo->p_payload = (char *)(fifo->p_shm + 1);
        fifo->p_shm->blksize = blksize;
        fifo->p_shm->blocks = blocks;
        fifo->p_shm->rd_idx = 0;
        fifo->p_shm->wd_idx = 0;
    }
    else /*如果打开共享内存成功，说明共享内存已经存在了*/
    {
        fifo->shmid = shmid;
        fifo->p_shm = (shmhead_t *)shmat(fifo->shmid, NULL, 0);
        if (fifo->p_shm == (shmhead_t *)-1)
            ERR_EXIT("shmat");

        fifo->p_shm->key = key;

        fifo->sem_mutex = sem_open(key);
        fifo->sem_full = sem_open(key + 100);
        fifo->sem_empty = sem_open(key + 200);

        fifo->p_payload = (char *)(fifo->p_shm + 1);
    }

    return fifo;
}
