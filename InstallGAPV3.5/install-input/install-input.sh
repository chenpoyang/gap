#!/bin/bash
basepath=$(cd `dirname $0`; pwd)
echo $basepath
####### FIREWALL ###############
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=8080/tcp --permanent
firewall-cmd --reload
######### NET-TOOLS ##############
cd $basepath/pkg-net-tools
rpm -ivh --force *.rpm
########## NETWORK #############
sed -i 's/BOOTPROTO=dhcp/BOOTPROTO=static/g' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i 's/IPV6INIT=yes/IPV6INIT=no/g' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i 's/ONBOOT=no/ONBOOT=yes/g' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i '$a IPADDR=192.168.2.10' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i '$a GATEWAY=192.168.2.1' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i '$a NETMASK=255.255.255.0' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
mac1=$(cat /sys/class/net/enp0s31f6/address)
echo $mac1
sed -i '$a HWADDR='"$mac1"'' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sleep 2
##
sed -i 's/BOOTPROTO=dhcp/BOOTPROTO=static/g' /etc/sysconfig/network-scripts/ifcfg-enp6s0
sed -i 's/IPV6INIT=yes/IPV6INIT=no/g' /etc/sysconfig/network-scripts/ifcfg-enp6s0
sed -i 's/ONBOOT=no/ONBOOT=yes/g' /etc/sysconfig/network-scripts/ifcfg-enp6s0
sed -i '$a IPADDR=192.168.2.11' /etc/sysconfig/network-scripts/ifcfg-enp6s0
sed -i '$a GATEWAY=192.168.2.1' /etc/sysconfig/network-scripts/ifcfg-enp6s0
sed -i '$a NETMASK=255.255.255.0' /etc/sysconfig/network-scripts/ifcfg-enp6s0
mac2=$(cat /sys/class/net/enp6s0/address)
echo $mac2
sed -i '$a HWADDR='"$mac2"'' /etc/sysconfig/network-scripts/ifcfg-enp6s0
sleep 2
systemctl stop NetworkManager
systemctl disable NetworkManager
systemctl restart network
########### JDK ############
cd $basepath/pkg-jdk
rpm -ivh --force *.rpm
######## MARIADB #########
cd $basepath/pkg-mariadb
# only used mariadb10
#yum remove -y mariadb-libs
rpm -ivh --force *.rpm
###
# only used mariadb10
#systemctl start mysql
# only used mariadb5
systemctl enable mariadb
systemctl start mariadb
mysql <<EOF
DROP DATABASE IF EXISTS gapwebmin;
CREATE DATABASE gapwebmin DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
EOF
######### FB ##########
cd $basepath
#rpm -ivh --force *.rpm
cp -rf pkg-fb-include/* /usr/local/include
#cp -rf directfb/* /usr/local/include
#cp -rf directfb-internal/* /usr/local/include
cd $basepath
cp -rf pkg-fb-lib/* /usr/lib
cd /usr/lib
ln -s lib++dfb-1.7.so.0.0.0 lib++dfb.so
ln -s lib++dfb-1.7.so.0.0.0 lib++dfb-1.7.so.0
ln -s libdirect-1.7.so.0.0.0 libdirect.so
ln -s libdirect-1.7.so.0.0.0 libdirect-1.7.so.0
ln -s libdirectfb-1.7.so.0.0.0 libdirectfb.so
ln -s libdirectfb-1.7.so.0.0.0 libdirectfb-1.7.so.0
ln -s libfusion-1.7.so.0.0.0 libfusion.so
ln -s libfusion-1.7.so.0.0.0 libfusion-1.7.so.0
######### libconfig ##########
cd $basepath
#rpm -ivh --force *.rpm
cp -rf pkg-config-include/* /usr/local/include
cd $basepath
cp -rf pkg-config-lib/* /usr/local/lib
cd /usr/local/lib
ln -s libconfig.so.9.2.0 libconfig.so
ln -s libconfig.so.9.2.0 libconfig.so.9
ln -s libconfig++.so.9.2.0 libconfig++.so
ln -s libconfig++.so.9.2.0 libconfig++.so.9
sed -i '$a /usr/local/lib' /etc/ld.so.conf
sed -i '$a /usr/lib' /etc/ld.so.conf
sleep 2
/sbin/ldconfig
########## FB-DEV ############
#cd $basepath/pkg-fb-dev
#rpm -ivh --force *.rpm
########### FB-SET ##############
#cd $basepath/pkg-fb-set
#rpm -ivh --force *.rpm
########## GAP #############
mkdir /home/update_dir
cd $basepath
\cp -rf GAP_IN /home
cd /home/GAP_IN
tar -xzvf GAPJetty.tar.gz
cd GAPJetty/webapps
mkdir GAPWebmin
cd GAPWebmin
jar -xvf /home/GAP_IN/GAPWebmin_input.war
cd /home/GAP_IN
rm -f GAPJetty.tar.gz
rm -f GAPWebmin.war
cd /home
chmod -R 777 GAP_IN
################ start on boot ##############################
sed -i '$a sh /home/GAP_IN/start.sh' /etc/rc.d/rc.local
chmod 777 /etc/rc.d/rc.local

echo "###############################################################"
echo "###################### INSTALL END ############################"
echo "###############################################################"
